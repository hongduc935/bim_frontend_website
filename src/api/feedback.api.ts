import Repository from "../congfig/repository/RepositoryConfig";
import { DomainServer } from "constant/Constant"
const url=DomainServer+'/api/v1/feedback'

const FeedbackAPI ={

    getFeedback:async ()=>{
        return await Repository("POST",url,{params:{},body:{}})
    },
    createFeedback:async (data:any)=>{
        return await Repository("POST",url+"/create",{params:{},body:data})
    },

}
export default FeedbackAPI