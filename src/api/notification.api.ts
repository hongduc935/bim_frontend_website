import Repository from "../congfig/repository/RepositoryConfig"
import { DomainServer } from "constant/Constant"
const url=DomainServer+'/api/v1/notification'

const NotificationAPI ={
    getNotificationByID:async (id:string)=>{
        return await Repository("GET",url+`/${id}`,{params:{},body:{}})
    },
    sendNotification:async (data:any)=>{
        return await Repository("POST",url,{params:{},body:data})
    }
   
}
export default NotificationAPI