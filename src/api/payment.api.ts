import Repository from "../congfig/repository/RepositoryConfig"
import CoreModel from "model/Core"
import { DomainServer } from "constant/Constant"
const url=DomainServer+'/api/v1/payment'

const PaymentAPI ={
    createPayment:async (data:any)=>{
        return await Repository("POST",url+"/create",{params:{},body:data})
    },
    
}
export default PaymentAPI