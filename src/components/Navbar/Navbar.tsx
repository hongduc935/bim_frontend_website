import React from 'react'
import styles from './styles/navbar.module.scss'
import {LogoutAction} from "redux/action/Auth/Auth"
import {useAppDispatch} from "redux/store"
import {useNavigate} from "react-router-dom"
import  {io} from "socket.io-client"
import ItemNotification from './component/ItemNotification/ItemNotification'
import NotificationAPI from 'api/notification.api'
import Image from 'components/Image/Image'
import User from 'constant/User'
const Navbar=()=>{
    const [socket, setSocket] = React.useState<any>(null);
    const [showNotification, setShowNotification] = React.useState<Boolean>(false)

    const [allNotification,setAllNotification] = React.useState([])

    const FetchAllNotification = React.useCallback(async()=>{
        return await NotificationAPI.getNotificationByID(User.id)
    },[])

    const dispatch = useAppDispatch()
    const navigate = useNavigate()
    const handleLogout=()=>{
        dispatch(LogoutAction())
        navigate('/')
    }
     React.useEffect(() => {
        const socket = io('http://localhost:5000')
        console.log(socket)
        console.log(socket.on("notification",(data)=>{
            console.log(data)
        }))

      }, []);
      React.useEffect(() => {
        FetchAllNotification().then((result:any)=>{
            setAllNotification(result?.data)
        })

      }, []);

      // subscribe to socket date event
    const subscribeToDateEvent = (interval = 1000) => {
        socket.emit('subscribeToDateEvent', interval);
    }
    return (
        <div className={`${styles.navbar} item-btw`}>
            <div className={`${styles.search} item-center`}>
                <Image contain={"contain"} image={"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQRHf8xTkqlVIQEv54SEwgEbWuqPPAiK_Dz0VhJig8QTwyahzQkve-bHCtM1gqxkRZldFo&usqp=CAU"}/>
                <div className={styles.line}>FHQ LMS</div>
            </div>
            <div className={styles.action}>      
                <div className={`${styles.item} `}>
                    <i className="fa-solid fa-bell" onClick={()=> setShowNotification(!showNotification)}/>
                    <div className={styles.listNotification} style={!showNotification  ? {visibility:"hidden"} :{visibility:"visible"}}>
                           {
                             allNotification?.length > 0 ? allNotification.flatMap((value:any,index:number)=>{
                                return  (<div key={`notification ${index}`}>
                                 <ItemNotification data ={value} />
                                </div>)  
                             }):<></>
                           }
                    </div>
                </div> 
                <div className={`${styles.item} ${styles.account} item-center `}>
                    Account
                    <div className={styles.option}>
                        <div className={styles.select}>
                            Profile
                        </div>
                        <div className={styles.select} onClick={handleLogout}>
                            Logout
                        </div>
                        <div className={styles.select}>
                            Cancel
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Navbar