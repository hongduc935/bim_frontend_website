import styles from './styles/break-crumb.module.scss'

const BreakCrumb = (props:any) => {
  return (
    <div className={`${styles.break} container`}>
        <p>{props?.content}</p>
    </div>
  )
}

export default BreakCrumb