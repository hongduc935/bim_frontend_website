import React from 'react'
import {Helmet} from "react-helmet";
const MetaSeo = ({title,description}:{title:string,description:string}) => {
  return (
    <Helmet>
        <meta charSet="utf-8" />
        <title>{title}</title>
        <link rel="canonical" href="http://mysite.com/example" />
        <meta name="description" content={description}/>
    </Helmet>
  )
}

export default MetaSeo