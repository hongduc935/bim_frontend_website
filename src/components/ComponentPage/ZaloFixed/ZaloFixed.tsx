import Image from 'components/Image/Image'
import styles from './styles/zalo-fixed.module.scss'
import ZaloImage from 'asset/images/zalo-icon.png'
const ZaloFixed = () => {
  return (
    <div className={styles.zaloFixed}>
        <a href="https://zalo.me/0987904586" target='blank'>
            <div className={styles.imageZalo}>
                <Image image={ZaloImage} contain='contain'/>
            </div>
        </a>
    </div>
  )
}

export default ZaloFixed