import styles from './styles/footer.module.scss'
const Footer = () => {
    return (
        <div className={`${styles.footer} container`}>
            <div className={`${styles.mainFooter} item-btw`}>
                <div className={`${styles.item1}  ${styles.item}`}>
                    <p className={styles.title}>HQL – Revit Mep Plugins</p>
                    <p className={styles.note}>Địa chỉ: 77 võ văn kiệt , phường An Lạc, Quận Bình tân</p>
                    <p className={styles.note}>Fangpage : https://www.facebook.com/bimsmart.hql</p>
                    <p className={styles.note}>Hotline 0987 904 586</p>
                    <p className={styles.note}>E-mail: bimsmart.hql@gmail.com</p>
                    <p className={styles.title}>TRUYỀN THÔNG VÀ XÃ HỘI</p>
                    <ul className={`${styles.icon} `}>
                        <li>
                        <i className="fa-brands fa-facebook"></i>
                        </li>
                        <li>
                        <i className="fa-brands fa-twitter"></i>
                        </li>
                        <li>
                        <i className="fa-brands fa-instagram"></i>
                        </li>
                        <li>
                        <i className="fa-brands fa-linkedin-in"></i>
                        </li>
                        <li>
                        <i className="fa-brands fa-instagram"></i>
                        </li>
                        <li>
                        <i className="fa-brands fa-linkedin-in"></i>
                        </li>
                    </ul>
                    <ul className={`${styles.icon} `}>
                        <li>
                        <i className="fa-brands fa-facebook"></i>
                        </li>
                    </ul>
                </div>
                <div className={`${styles.item1}  ${styles.item}`}>
                    <p className={styles.title}>THÔNG TIN CHUNG</p>
                    <p className={styles.note}>Giới thiệu</p>
                    <p className={styles.note}>Sản phẩm</p>
                    <p className={styles.note}>Tin Tức</p>
                    <p className={styles.note}>Liên hệ</p>
                    <p className={styles.note}>Điều khoản và điều kiện</p>
                    <p className={styles.note}>Chính sách bảo mật thông tin</p>
                </div>
                <div className={`${styles.item1}  ${styles.item}`}>
                    <p className={styles.title}>HQL - Revit Mep Plugins</p>
                    <p className={styles.note}>Quy định về đặt mua sản phẩm, dịch vụ</p>
                    <p className={styles.note}>Hướng dẫn thanh toán và thủ tục hoàn tiền</p>
                    <p className={styles.note}>Hướng dẫn xử lý sự cố và khiếu nại</p>
                    <p className={styles.note}>Đăng ký làm đại lý</p>
                </div>
            </div>
            <div className={`${styles.copyright} item-btw`}>
                <div className={styles.source}>
                    <p className={styles.shareContent}>© 2022 Bản quyền thuộc về HQL – Revit Mep Plugins</p>
                </div>
                <div className={`${styles.right} item-center`}>
                    <p className={styles.shareContent}>Điều khoản và điều kiện</p>
                    <p className={styles.shareContent}> Chính sách bảo mật thông tin</p>
                </div>

            </div>
          
        </div>
    )
}

export default Footer;