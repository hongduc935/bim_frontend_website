import Image from 'components/Image/Image'
import styles from './styles/header.module.scss'
import {NavLink, useNavigate} from "react-router-dom"
import LogoBim from 'asset/images/Logo_Bim.png'
import GetWinDown from 'utility/SizeWindow'
import { useState } from 'react'
import NavigationOnPhone from './components/NavigationOnPhone/NavigationOnPhone'
import axios from 'axios'
const Header = () => {


    let [hiddleFolderTree1,setHiddenFolderTree1]= useState<Boolean>(false)
    let [hiddleFolderTree2,setHiddenFolderTree2]= useState<Boolean>(false)
    let [hiddleFolderTree3,setHiddenFolderTree3]= useState<Boolean>(false)



    const navigate = useNavigate()

    const handleNavigate =(string:string)=>{
        navigate(string)
        window.location.reload();
    }
    let [showNavigation,setShowNavigation] = useState<Boolean>(false)

    const handleCloseNavigationPhone = ()=>{
        setShowNavigation(false)
    }
    const handleOpenNavigationPhone = ()=>{
        setShowNavigation(true)
    }

    const handleHiddenFolderTree1 = ()=>{
        setHiddenFolderTree1(!hiddleFolderTree1)
    }
    const handleHiddenFolderTree2 = ()=>{
        setHiddenFolderTree2(!hiddleFolderTree2)
    }

    const handleHiddenFolderTree3 = ()=>{
        setHiddenFolderTree3(!hiddleFolderTree3)
    }
    
    const handleUpdateCountDownload = ()=>{

        axios.post("https://hqltools.com/api/v1/setting/update-download",{})  

    }


    return (
        <div className={styles.header}>
            <header>
                <div className={`${styles.headerLogo} item-center container`}>
                    <div className={styles.image}>
                        <Image contain='contain' image= {LogoBim}/>
                    </div>
                    <div className={styles.info}>
                        <p className={styles.name}>HQL - Revit Mep Plugins</p>
                        <p className={styles.phone}>Hotline <span>0987 904 586</span></p>
                    </div>
                </div>

                <div className={styles.headerOption}>
                    {GetWinDown() == 1 ? ( <div className={`${styles.iconBar} item-center`}>
                        {
                            showNavigation ?  <i className="fa-solid fa-xmark" onClick={handleCloseNavigationPhone}/> : <i  className="fa-solid fa-bars" onClick={handleOpenNavigationPhone}/>
                        }
                    </div>) :  <ul>
                        <li><NavLink to={'/'}>Trang chủ</NavLink></li>
                        <li className={styles.product}> 
                            <NavLink to={'/product'}>Sản phẩm &nbsp; <i className="fa-solid fa-caret-down"></i></NavLink>
                            <div className={styles.titleProduct}>
                                <ul className={styles.listItem}>
                                    <li >
                                        <p className={`item-btw w-90`} onClick={handleHiddenFolderTree1}><span>1. HqlTools  - Addin For Revit Mep</span> <span><i className="fa-solid fa-angle-down"></i></span></p>
                                        <ul className={`${styles.lv2} ${hiddleFolderTree1 === true ? "hiddenlv2":"nothiddenlv2"}`} >
                                            <li className={styles.lv2_li} onClick={()=>handleNavigate("/product?item=Revit_Mep-Revit_Mep1&&page=Addin-For-Revit-Mep_Modelling-Tools")}>Modelling Tools</li>
                                            <li className={styles.lv2_li} onClick={()=>handleNavigate("/product?item=Revit_Mep-Revit_Mep2&&page=Addin-For-Revit-Mep_Drawing-Tools")}>Drawing Tools</li>
                                            <li className={styles.lv2_li} onClick={()=>handleNavigate("/product?item=Revit_Mep-Revit_Mep3&&page=Addin-For-Revit-Mep_Quantification-Tools")}>Quantification Tools</li>
                                            <li className={styles.lv2_li} onClick={()=>handleNavigate("/product?item=Revit_Mep-Revit_Mep4&&page=Addin-For-Revit-Mep_Parameter-Tools")}>Parameter Tools</li>
                                        </ul>
                                    </li>
                                    <li >
                                        <p className={`item-btw w-90`} onClick={handleHiddenFolderTree2}><span>2. HqlTools - Sản Phẩm Tương Lai</span><span><i className="fa-solid fa-angle-down"></i></span></p>
                                        <ul className={`${styles.lv2} ${hiddleFolderTree2 === true ? "hiddenlv2":"nothiddenlv2"}`}>
                                            <li className={styles.lv2_li} onClick={()=>handleNavigate("/product?item=Product_Features-Product_Features1&&page=Sản-Phẩm-Tương-Lai_Sản-Phẩm-Tương-Lai-1")}>Sản Phẩm Tương Lai 1 </li>
                                            <li className={styles.lv2_li} onClick={()=>handleNavigate("/product?item=Product_Features-Product_Features2&&page=Sản-Phẩm-Tương-Lai_Sản-Phẩm-Tương-Lai-2")}>Sản Phẩm Tương Lai 2 </li>
                                            <li className={styles.lv2_li} onClick={()=>handleNavigate("/product?item=Product_Features-Product_Features3&&page=Sản-Phẩm-Tương-Lai_Sản-Phẩm-Tương-Lai-3")}>Sản Phẩm Tương Lai 3</li>
                                            <li className={styles.lv2_li} onClick={()=>handleNavigate("/product?item=Product_Features-Product_Features3&&page=Sản-Phẩm-Tương-Lai_Sản-Phẩm-Tương-Lai-4")}>Sản Phẩm Tương Lai 3</li>
                                        </ul>
                                    </li>
                                    <li>
                                        <p className={`item-btw w-90`} onClick={handleHiddenFolderTree3}><span>3. Download Tools</span><span><i className="fa-solid fa-angle-down"></i></span></p>
                                        <ul className={`${styles.lv2} ${hiddleFolderTree3 === true ? "hiddenlv2":"nothiddenlv2"}`}>
                                        <li className={styles.lv2_li} onClick={handleUpdateCountDownload} ><a href="https://files-accl.zohoexternal.com/public/workdrive-external/download/kgpnu88170983a596430f920756668098fab7?x-cli-msg=%7B%22linkId%22%3A%226xapI8Slm5j-VuuDZ%22%2C%22isFileOwner%22%3Afalse%2C%22version%22%3A%221.0%22%7D" download="download"><p className={`item-btw w-90`}><span>File Cài đặt</span><span><i className="fa-solid fa-download"/></span></p></a></li>
                                            <li className={styles.lv2_li} ><a href="https://www.youtube.com/watch?v=i-jvACRwsJc" target="_blank"><p className={`item-btw w-90`}><span>Video Hướng dẫn</span><span><i className="fa-solid fa-download"/></span></p></a></li>
                                        </ul>
                                    </li>

                                </ul>
                               
                            </div>
                        </li>
                        <li> <NavLink to={'/library'}>Thư Viện Hỗ Trợ</NavLink></li>
                        <li> <NavLink to={'/introduce'}>Giới thiệu</NavLink></li>
                        <li> <NavLink to={'/contact'}>Liên hệ</NavLink></li>
                        <li className={styles.account}> <NavLink to={'/login'}><i className="fa-solid fa-user-large"/></NavLink>
                            <div className={styles.option}>
                                <p> <NavLink to={'/login'}>Đăng kí</NavLink>  </p>
                                <p>Đăng nhập</p>
                            </div>
                        </li>
                    </ul>}   
                </div>
            </header>
            {showNavigation ? <NavigationOnPhone /> :<></>}
        </div>
    )
}

export default Header