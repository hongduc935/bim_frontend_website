import { useState } from 'react'
import styles from './styles/navigation-on-phone.module.scss'
import { useNavigate } from 'react-router-dom'


const NavigationOnPhone = () => {



  let [hiddleFolderTree1,setHiddenFolderTree1]= useState<Boolean>(false)
  let [hiddleFolderTree2,setHiddenFolderTree2]= useState<Boolean>(false)
  const [folderTree,setFolderTree] = useState<Boolean>(false)
  const navigate = useNavigate()

  const handleNavigate =(string:string)=>{
    navigate(string)
    window.location.reload();
  }
  const handleHiddenFolder =()=>{
    setFolderTree(!folderTree)
  }
  const handleHiddenFolderTree1 = ()=>{
    setHiddenFolderTree1(!hiddleFolderTree1)
  }
  const handleHiddenFolderTree2 = ()=>{
      setHiddenFolderTree2(!hiddleFolderTree2)
  }
  return (
    <div className={styles.mainNavigationPhone}>
        <ul>
            <li className='item-btw' onClick={()=>handleNavigate("/")}>
              <span>Trang Chủ</span> 
              <span><i className="fa-solid fa-house"/></span>
            </li>
            <li  className='item-btw' onClick={handleHiddenFolder}>
                <span>Product</span> <span><i className="fa-solid fa-comment"/></span> 
            </li>
            {
                  folderTree ? 
                  (
                    <li className={styles.special}>
                          <ul className={styles.folderTree} style={{
                          top:'10px'
                        }}>
                          <li >
                          <p className={`item-btw w-100`}  onClick={handleHiddenFolderTree1}><span>1. HqlTools - Addin For Revit Mep</span> <span><i className="fa-solid fa-angle-down"></i></span></p>
                              <ul className={`${styles.lv2} ${hiddleFolderTree1 === true ? "hiddenlv2":"nothiddenlv2"}`} >
                                <li className={styles.lv2_li} onClick={()=>handleNavigate("/product?item=Revit_Mep-Revit_Mep1&&page=Addin-For-Revit-Mep_Modelling-Tools")}>Modelling Tools </li>
                                <li className={styles.lv2_li} onClick={()=>handleNavigate("/product?item=Revit_Mep-Revit_Mep2&&page=Addin-For-Revit-Mep_Drawing-Tools")}>Drawing Tools</li>
                                <li className={styles.lv2_li} onClick={()=>handleNavigate("/product?item=Revit_Mep-Revit_Mep3&&page=Addin-For-Revit-Mep_Quantification-Tools")}>Quantification Tools</li>
                                <li className={styles.lv2_li} onClick={()=>handleNavigate("/product?item=Revit_Mep-Revit_Mep4&&page=Addin-For-Revit-Mep_Parameter-Tools")}>Parameter Tools</li>
                              </ul>
                          </li>

                          <li >
                          <p className={`item-btw w-100`}  onClick={handleHiddenFolderTree2}><span>2. HqlTools - Sản Phẩm Tương Lai</span><span><i className="fa-solid fa-angle-down"></i></span></p>
                            <ul className={`${styles.lv2} ${hiddleFolderTree2 === true ? "hiddenlv2":"nothiddenlv2"}`}>
                                <li className={styles.lv2_li} onClick={()=>handleNavigate("/product?item=Product_Features-Product_Features1&&page=Sản-Phẩm-Tương-Lai_Sản-Phẩm-Tương-Lai-1")}>Sản Phẩm Tương Lai 1</li>
                                <li className={styles.lv2_li} onClick={()=>handleNavigate("/product?item=Product_Features-Product_Features1&&page=Sản-Phẩm-Tương-Lai_Sản-Phẩm-Tương-Lai-2")}>Sản Phẩm Tương Lai 2</li>
                                <li className={styles.lv2_li} onClick={()=>handleNavigate("/product?item=Product_Features-Product_Features1&&page=Sản-Phẩm-Tương-Lai_Sản-Phẩm-Tương-Lai-3")}>Sản Phẩm Tương Lai 3</li>
                                <li className={styles.lv2_li} onClick={()=>handleNavigate("/product?item=Product_Features-Product_Features1&&page=Sản-Phẩm-Tương-Lai_Sản-Phẩm-Tương-Lai-3")}>Sản Phẩm Tương Lai 4</li>
                            </ul>
                            
                          </li>
                          <li>
                              <a href="https://drive.google.com/drive/folders/1Eel1yraRDdUMva7fmA4-vLINV9luMT0r" target="_blank"><p className={`item-btw w-100`}><span>3. Download Tools</span><span><i className="fa-solid fa-download"/></span></p></a>
                          </li>
                        </ul>
                      </li>
                      ):
                      <></>
            }
            <li  className='item-btw' onClick={()=>handleNavigate("/library")}><span>Thư Viện Hỗ Trợ</span> <span><i className="fa-solid fa-book"/></span></li>
            <li  className='item-btw' onClick={()=>handleNavigate("/contact")}><span>Liên Hệ</span> <span><i className="fa-solid fa-phone"/></span></li>
            <li  className='item-btw' onClick={()=>handleNavigate("/introduce")}><span>Giới Thiệu</span> <span><i className="fa-solid fa-globe"/></span></li>
            <li  className='item-btw' onClick={()=>handleNavigate("/login")}><span>Đăng Kí</span> <span><i className="fa-solid fa-user-plus"/></span></li>
        </ul>
    </div>
  )
}

export default NavigationOnPhone