import MetaSeo from "components/MetaSeo/MetaSeo"
import Footer from "./Footer/Footer"
import Header from "./Header/Header"
import ScrollToTop from "./ScrollToTop/ScrollToTop"
import ZaloFixed from "./ZaloFixed/ZaloFixed"
const ComponentPage=(props:any)=>{

    const seo = props?.seo
    return (
        <>
            <MetaSeo title={seo?.title}  description={seo?.description}/>
            {props.isHiddenHeader===true&&<Header/>}
            {props.children}
            {props.isHiddenScroll===true&&<ScrollToTop/>}
            {props.isHiddenZalo===true&&<ZaloFixed/>}
            {props.isHiddenFooter===true&&<Footer/>}
        </>
    )
}
export default ComponentPage;