import {useEffect} from 'react'
import styles from './styles/scroll-to-top.module.scss'

const ScrollToTop = () => {
    useEffect(() => {
        window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
      }, []);
  return (
    <button
        onClick={() => {
        window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
        }}
        className={`${styles.buttonScrollTop} item-center`}
        style={{
        position: 'fixed',
        fontSize: '20px',
        bottom: '40px',
        right: '40px',
        color: '#fff',
        textAlign: 'center',
        }}
    >
   <i className="fa-solid fa-chevron-up"/>
  </button>
  )
}

export default ScrollToTop