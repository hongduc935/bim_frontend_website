interface HistoryReport{
    name_task:string
    week:string
    percent:string
    status:number
}
export default HistoryReport