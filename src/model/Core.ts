interface CoreModel{
    infact:Number|0// thực tiễn đề tài 
    completed:Number|0, // khả năng hoàn thiện của đề tài
    quality_present:Number|0,// chất lượng thuyết trình
    research:Number|0,// điểm nghiên cứu
    content_synthesis:Number|0,// điểm tổng hợp nội dung luận văn
    quality_thesis:Number|0,// chất lượng của luận văn
    reply_question:Number|0,// điểm trả lời câu hỏi
}
export default  CoreModel