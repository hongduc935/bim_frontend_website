// export const DomainServer = "https://backendbim.onrender.com"
// export const DomainServer = "http://localhost:5000"
export const DomainServer = "https://hqltools.com"
// export const DomainServer = "http://3.27.89.89"

export const HomePage = {

}

export const ProductPage = {
    
}

export const MetaSeo = {
    HomePage:{
        title:"HQL - Revit Mep Plugins",
        description:"Chuyên cung cấp các tool , giải pháp xây dựng bản vẽ 1 cách tiện lợi .  Sản phẩm hỗ trợ các công cụ mạnh mẽ như Revit mep, addin revit, tự động hoá , sử dụng các bên thứ 3 một cách mạnh mẽ như revit api. "
    },
    ProductPage: {
        title: "HQL - Revit MEP Plugins | Plugin, Add-on, Extension for Revit",
        description: "HQL là giải pháp tích hợp nhiều công cụ hỗ trợ tiện lợi cho người sử dụng Revit. Sản phẩm cung cấp các plugin, add-on và extension mạnh mẽ cho Revit MEP, giúp tối ưu hóa quy trình làm việc và tăng hiệu suất. Khám phá các công cụ tự động hóa và sử dụng Revit API để mở rộng tính năng của Revit một cách hiệu quả."
    }
    
}