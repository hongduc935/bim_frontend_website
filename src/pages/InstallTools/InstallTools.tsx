import styles from './styles/install-tools.module.scss'
import ComponentPage from 'components/ComponentPage/ComponentPages'
import Image from 'components/Image/Image'
const InstallTools = () => {
  return (
    <div className={styles.mainInstall}>
        <ComponentPage
                    isHiddenHeader={true} 
                    isHiddenFooter={true}
                    isHiddenScroll = {true}
                    isHiddenZalo = {true}
                >
                    <div className={styles.titleInstall}>
                        <p>Hướng dẫn Install App</p>
                    </div>
                    <section className={`${styles.stepOne} item-btw container`}>
                            <div className= {styles.stepImage}>
                                <Image image='https://tinhhoacongnghe.com/wp-content/uploads/2021/08/cach-chuyen-hinh-anh-thanh-van-ban-bang-google-drive-anh-3.png'/>
                            </div>
                            <div className= {styles.stepTutorial}>
                                <p>Bước 1: Truy cập vào đường link drive tại đây để Download file cài đặt</p>
                            </div>
                    </section>
        </ComponentPage>
    </div>
  )
}

export default InstallTools