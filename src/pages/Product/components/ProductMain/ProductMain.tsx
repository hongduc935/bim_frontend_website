import  { useEffect, useState } from 'react'
import styles from './styles/product-main.module.scss'
import { useSearchParams } from 'react-router-dom'
import DB from '../ProductReview/FakeDB'
import ReactPlayer from 'react-player'
import GetWinDown from 'utility/SizeWindow'

const ProductMain = () => {

    const [params]=useSearchParams()

    let pagesString:string=params.get('item') ||"Revit_Mep-Revit_Mep1"

    let pages= params.get("page")||"Addin-For-Revit-Mep_Modelling-Tools"

    let [listProduct,setListProduct] = useState<any>([])

    useEffect(()=>{
        let handleParamDB :string[]= pagesString?.split("-")
        setListProduct(DB[handleParamDB[0]][handleParamDB[1]])
    },[pagesString,pages])

    return (
        <div className={`${styles.productMain} container`}>
            <div className={styles.titlePage}>
                <h1  className={styles.title}><i className="fa-solid fa-screwdriver-wrench"/> &nbsp; {pages.split("_")[0].replaceAll("-"," ")+" / " + pages.split("_")[1].replaceAll("-"," ")} </h1>
            </div>
            {
                listProduct.length > 0  ? 
                listProduct.flatMap((value:any,index:number)=>{
                    return <div className={`${styles.mainSlide} item-btw w-100`} key={`item`+index}>
                    <div className={`${styles.cart} w-100 item-btw`} >
                            <div className={`${styles.image} w-50`}>
                                <ReactPlayer
                                    width={"100%"}
                                    height={ GetWinDown() === 1 ? 250 :400}
                                            url={value.link}
                                        />
                            </div>
                            <div className={`${styles.mainPost} w-50`}>
                                <h2 className={styles.title1}>{value.title}</h2>
                                <div></div>
                                {
                                    value?.content?.length > 0 ? value?.content?.flatMap((value_content:any,index:Number)=>{
                                        return <p  title={value_content} className={styles.content} key={`list_content`+index}><i className="fa-solid fa-check"></i> &nbsp;{ GetWinDown() === 1 ? value_content.slice(0,200) : value_content+" ..."}<br/></p>
                                    }):<></>
                                }
                            </div>
                            
                    </div>
                </div>
                }) :<></>
            }
        </div>
    )
}

export default ProductMain