import styles from './styles/preview.module.scss'
import ReactPlayer from 'react-player'

const Preview = (props:any) => {

  return (
    <div className={`${styles.preview} item-center`}>
        <div className={styles.close} onClick={props?.handleClose}>
            <div className={styles.closemain}>
                <i className="fa-solid fa-xmark"></i>
            </div>
        </div>
        <div className={styles.mainPreview}>
                      <ReactPlayer
                          width={"100%"}
                          height={600}
                          playing={true}
                                  url={props?.link}
                              />
        </div>
        Preview
    </div>
  )
}

export default Preview