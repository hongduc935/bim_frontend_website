const FakeDB:any = {
    "Revit_Mep":{
        "Revit_Mep1":[
            {
                "title":"Đặt Family Revit",
                "content":[
                    "Tool này cho phép người dùng chuyển từ block cad  sang family Revit  một cách chính xác và nhanh chóng ",
                    "Điều kiện khi sự dụng tool này : ","File cad của bạn phải có Block cad ",
                    "Tâm của block cad phải đặt đúng tọa độ (0,0)",
                    "Lợi ích của tool này khi bạn sử dụng đó là nó sẽ tự động tìm vị trí của block cad rồi đặt family đó theo đúng vị trí"
                ],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM",
            },
            {
                "title":"Dựng Hệ Thống Mep Từ File Cad",
                "content":[
                    "Tool cho phép người dùng pick chọn line từ file cad đã có sẵn link vào để tạo ống bằng cách khai báo các thông tin trong giao diện ",
                    "Điều kiện của khi sử dụng tool này là bạn phải có file cad thiết kế cơ điện. ",
                    "Khi link file cad vào thì không được move file cad","Lợi ích của tool này là chuyển từ file cad sang 3D nhanh chóng và chính xác theo đường line file cad chỉ bằng 1 cú click phù hợp cho việc model 3D hệ thống cơ điện và đặc biệt đối với đường ống thoát. Rất nhanh chóng và tiện lợi."
                ],
                "link":"https://www.youtube.com/watch?v=CvU88ZByinI",
            },
            {
                "title":"Đặt Support / Hanger",
                "content":[
                    "Tool này cho phép người dùng lắp đặt hanger cho đường ống 1 cách nhanh chóng và chính xác ",
                    "Tool cho phép hanger bám vào sàn/dầm hoặc không ",
                    "Tool giúp các kỹ sư linh hoạt trong quá trình đạt hanger, lựa chọn hanger hợp lý để tiến hành đặt ",
                ],
                "link":"https://www.youtube.com/watch?v=ro8CcX0DRdo"
                
            },
            {
                "title":"Kết Nối Thiết bị vệ sinh ",
                "content":[
                    "Với bộ tool này thì sẽ là giải pháp cho các kỹ sư thiết kế và Modelling cho hệ thống cấp thoát nước ",
                    "Việc kết nối ống vào thiết bị vệ sinh luôn là đề tài khó khăn cho các bạn.. các bạn phải suy nghĩ hướng đi, kiểu kết nối. vậy thì bội tool này đưa ra cho các bạn các giải pháp thiết kế đường đi và kiểu kết nối ",
                    "Chỉ cần lựa chọn kiểu hướng đi và kiểu kết nối thì bạn có thể connect từ thiết bị tới ống.",
                    "Thiết bị vệ sinh phải có connector parameter Flow Direction phải đúng (In/Out)"
                ],
                "link":"https://www.youtube.com/watch?v=2IugH7bYDb4",
            },
            {
                "title":"Kết Nối Ống Nhánh Vào Ống Chính ",
                "content":
                [
                    "Với trường hợp này thì ta có rất nhiều option kết nối. Cho phép ta linh động chọn hướng kết nối và kiểu kết nối theo tùy thiết kế. Với những địa hình phức tạp thì đây quả là tool rất phù hợp cho cả dân thiết kế và dựng hình mep",
                ],
                "link":"https://www.youtube.com/shorts/I3r61Fyc-dM",
            },

            {
                "title":"Cắt Ống",
                "content":
                [
                    "Tool này cho phép ngừi dùng chia ống chính xác theo chiều dài đã khai báo từ trước. Đặc biệt tool này là chia chính xác với từng loại union và người dùng có thể chọn hướng chia ống từ đầu hay từ cuối đoạn ống",
                    "Với chức năng sử dụng song song với Revit giúp bạn linh hoạt trong quá trình sử dụng, đây là điểm khác biệt so với những tool cùng loại trên thị trường."
                ],
                "link":"https://www.youtube.com/watch?v=1Xo-eq8hFyk",
            },

            {
                "title":"Kết Nối Đầu Phun Chữa Cháy",
                "content":
                [
                    "Tool này cho phép người dùng kết nối đầu phjun chữa cháy với ống chính rất dễ dàng và nhanh chóng",
                    "Có nhiều option hướng kết nối cho đầu phun hướng lên và hướng xuống và cả kết nối bằng ống nối mềm",
                    "Tool này còn có một phần cũng rất hay cho các kỹ sư thiết kế đó là phần chia size ống tự động theo tiêu chuẩn việt nam và NFPA 13",
                    "Hãy trải nghiệm tool này để thấy giá trị mang lại cho kỹ sư"
                ],
                "link":"https://www.youtube.com/watch?v=Z4nEXc2uXwE",
              
            },
            {
                "title":"Kết Nối Miệng Gió Vào Ống Gió",
                "content":
                [
                    "Tool này cho phep người dùng kết nối tự động miệng gió vào ống gió với nhiều option cho phần ống nối.. ",
                    "Có nhiều option hướng kết nối cho đầu phun hướng lên và hướng xuống và cả kết nối bằng ống nối mềm",
                    "Người dùng có thể chọn kết nối bằng ống cứng hay ống mền hoặc là vừa ống cứng vừa ống mềm bằng cách khai báo thông tin trên giao diện. ",
                    "Tool này đặc biệt là cho bạn chọn có gắn VCD vào hay không",
                    "Tool này rất phù hợp cho các kỹ sư thiết kế và model"
                ],
                "link":"https://www.youtube.com/watch?v=-K6JpHDLfDE",
            },

            {
                "title":"Đổi Hệ Thống",
                "content":[
                    "Trong Revit kỹ sư muốn đổi hệ thống cục bộ tại một vị trí nào đó thì có vẽ hơi khó khăn vì có thể nếu làm không đúng nó sẽ đổi toàn bộ hệ thống đường ống mà điều này không phải ý muốn của kỹ sư như lúc ban đầu. Do đó với tool này thì kỹ sư chỉ cần quét chọn 1 đoạn ống hay nhiều đoạn ống muốn đồi thì những đoạn ống đó thay đổi theo hệ thống đã cài đặt  nó sẽ không ảnh hưởng tới toàn bộ hệ thống. Rất tiện lợi và dễ dùng để phân biệt từng hệ thống với nhau.",
                ],
                "link":"https://www.youtube.com/watch?v=aBbyoM-Hpeo",
            },
        ],
        "Revit_Mep2":[
            {
                "title":"Các Tiện ích View",
                "content":[
                    "Nội dung 1 nè ",
                    "Nội dung 2 nè ",
                    "Nội dung 3 nè ",
                    "Nội dung 4 nè"
                ],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",

            },
            {
                "title":"Các Tiện Ích ghi chú thích",
                "content":[
                    "Nội dung 1 nè ",
                    "Nội dung 2 nè ",
                    "Nội dung 3 nè ",
                    "Nội dung 4 nè"
                ],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",
              },
            {
                "title":"Xóa các đối tượng trùng",
                "content":[
                    "Nội dung 1 nè ",
                    "Nội dung 2 nè ",
                    "Nội dung 3 nè ",
                    "Nội dung 4 nè"
                ],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",

            },
            {
                "title":"Clean model",
                "content":[
                    "Nội dung 1 nè ",
                    "Nội dung 2 nè ",
                    "Nội dung 3 nè ",
                    "Nội dung 4 nè"
                ],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",
             },

            {
                "title":"Add Thông tin vào dự án",
                "content":[
                    "Nội dung 1 nè ",
                    "Nội dung 2 nè ",
                    "Nội dung 3 nè ",
                    "Nội dung 4 nè"
                ],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",
            },
            {
                "title":"Tạo PipeType cho template",
                "content":[
                    "Nội dung 1 nè ",
                    "Nội dung 2 nè ",
                    "Nội dung 3 nè ",
                    "Nội dung 4 nè"
                ],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",
              }
        ],
        "Revit_Mep3":[
            {
                "title":"Tính tổng chiều dài ống",
                "content":[
                    "Nội dung 1 nè ",
                    "Nội dung 2 nè ",
                    "Nội dung 3 nè ",
                    "Nội dung 4 nè"
                ],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",
            },
            {
                "title":"Tạo Bảng Khối Lượng Theo Mẫu Boq",
                "content":[
                    "Khi bạn muốn thống kê khối lượng cho dự án mà muốn theo bảng BoQ của khách hàng thì phải làm sao. Có phải bạn sẽ thủ cong tạo ra tham số parameter như trong bảng BoQ, rồi…tùm lum chuyện hết rất mất thời gian phải không? thì đây quả là giải pháp giúp bạn tạo ra bảng khối lượng theo từng mẫu BoQ đáp ứng nhu cầu khách hàng. Bạn không phải cặm cụi thủ cong nữa.. Việc của bạn là chuẩn bi cài đặt và click chuột ..thế là xong. ",
                ],
                "link":"https://www.youtube.com/watch?v=CtEr22FVxbk",
            },
            {
                    "title":"Tính diện tích tole Fitting ống gió",
                    "content":[
                        "Nội dung 1 nè ",
                        "Nội dung 2 nè ",
                        "Nội dung 3 nè ",
                        "Nội dung 4 nè"
                    ],
                    "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",
            },
            {
                "title":"Tính diện tích tole Fitting ống gió",
                "content":["Nội dung 1 nè ","Nội dung 2 nè ","Nội dung 3 nè ","Nội dung 4 nè"],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",
            },

        ],
        "Revit_Mep4":[
            {
                "title":"Add Share Parameter vào dự án",
                "content":["Khi làm việc với một dự án Bim thì việc phải tạo ra tham số parameter để quản lý thông tin đối tượng là rất cần thiết. vấn đề dặt ra là làm sao để add đồng loạt vào dự án có phải ta thủ công add từng tham số như trong Revit không. Do đó tool này giúp cho kỹ sư tự động add đồng loạt tham số parameter từ 1 file txt có sẵn.  "],
                "link":"https://www.youtube.com/watch?v=yTqmS18vUmk",
 
            },
            {
                "title":"Add Data Vào Dự Án",
                "content":["Việc quản lý hệ thống/ filter đường ống là 1 phần quan trong khi làm Bim Mep. Đặc biệt là mỗi dự án có 1 BEP riêng thì lại là vấn đề. Cách làm bình thường là phải quản lý bằng file template cho từng khách hàng. Do đó tool add thông tin này sẽ giúp cho manager quản lý 1 cách thông minh chỉ cần 1 file excel thay vì phải tạo ra rất nhiều file template revit. Chỉ cần 1 file excel là có thể tạo ra rất nhiều template theo ý khách hàng.  "
                ,"Tool này phù hợp cho Bim manager để quản lý theo BEP. "],
                "link":"https://www.youtube.com/watch?v=uzm8O4UPMd0",
            },
            {
                "title":"Add Share Parameter vào dự án",
                "content":["Nội dung 1 nè ","Nội dung 2 nè ","Nội dung 3 nè ","Nội dung 4 nè"],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",
 
            },
            {
                "title":"Tạo file share parameter",
                "content":["Nội dung 1 nè ","Nội dung 2 nè ","Nội dung 3 nè ","Nội dung 4 nè"],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",
            }
        ]
    },
    "Product_Features":{
        "Product_Features1":[
            {
                "title":"Sản phẩm tương lai 1.1",
                "content":["Nội dung 1 nè ","Nội dung 2 nè ","Nội dung 3 nè ","Nội dung 4 nè"],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",
            },
            {
                "title":"Sản phẩm tương lai 41.2",
                "content":["Nội dung 1 nè ","Nội dung 2 nè ","Nội dung 3 nè ","Nội dung 4 nè"],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",
            },
            {
                "title":"Sản phẩm tương lai 1.1",
                "content":["Nội dung 1 nè ","Nội dung 2 nè ","Nội dung 3 nè ","Nội dung 4 nè"],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",
            },
            {
                "title":"Sản phẩm tương lai 41.2",
                "content":["Nội dung 1 nè ","Nội dung 2 nè ","Nội dung 3 nè ","Nội dung 4 nè"],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",
            }
        ],
        "Product_Features2":[
            {
                "title":"Sản phẩm tương lai 2.1",
                "content":["Nội dung 1 nè ","Nội dung 2 nè ","Nội dung 3 nè ","Nội dung 4 nè"],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",
            },
            {
                "title":"Sản phẩm tương lai 42.2",
                "content":["Nội dung 1 nè ","Nội dung 2 nè ","Nội dung 3 nè ","Nội dung 4 nè"],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s", 
            },
            {
                "title":"Sản phẩm tương lai 2.1",
                "content":["Nội dung 1 nè ","Nội dung 2 nè ","Nội dung 3 nè ","Nội dung 4 nè"],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",
            },
            {
                "title":"Sản phẩm tương lai 42.2",
                "content":["Nội dung 1 nè ","Nội dung 2 nè ","Nội dung 3 nè ","Nội dung 4 nè"],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s", 
            }
        ],
        "Product_Features3":[
            {
                "title":"Sản phẩm tương lai 3.1",
                "content":["Nội dung 1 nè ","Nội dung 2 nè ","Nội dung 3 nè ","Nội dung 4 nè"],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",
            },
            {
                "title":"Sản phẩm tương lai 3.2",
                "content":["Nội dung 1 nè ","Nội dung 2 nè ","Nội dung 3 nè ","Nội dung 4 nè"],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",
            },
            {
                "title":"Sản phẩm tương lai 2.1",
                "content":["Nội dung 1 nè ","Nội dung 2 nè ","Nội dung 3 nè ","Nội dung 4 nè"],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",
            },
            {
                "title":"Sản phẩm tương lai 42.2",
                "content":["Nội dung 1 nè ","Nội dung 2 nè ","Nội dung 3 nè ","Nội dung 4 nè"],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s", 
            }
        ],
        "Product_Features4":[
            {
                "title":"Sản phẩm tương lai 4",
                "content":["Nội dung 1 nè ","Nội dung 2 nè ","Nội dung 3 nè ","Nội dung 4 nè"],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",
            },
            {
                "title":"Sản phẩm tương lai 4 ,5",
                "content":["Nội dung 1 nè ","Nội dung 2 nè ","Nội dung 3 nè ","Nội dung 4 nè"],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",
            },
            {
                "title":"Sản phẩm tương lai 4 ,5",
                "content":["Nội dung 1 nè ","Nội dung 2 nè ","Nội dung 3 nè ","Nội dung 4 nè"],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",
            },
            {
                "title":"Sản phẩm tương lai 4 ,5",
                "content":["Nội dung 1 nè ","Nội dung 2 nè ","Nội dung 3 nè ","Nội dung 4 nè"],
                "link":"https://www.youtube.com/watch?v=7-tbc5NcQqM&t=70s",
            }
        ],
    }
}
export default FakeDB;