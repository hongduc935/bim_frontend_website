import React, { useEffect,useState } from 'react'
import styles from './styles/product-review.module.scss'
import { Swiper, SwiperSlide } from "swiper/react"
import GetWinDown from 'utility/SizeWindow'
import LoaderItem from 'components/LoaderItem/LoaderItem'
import ReactPlayer from 'react-player'
import Preview from '../Preview/Preview'
import ToolIcons from 'asset/images/tools_icons.jpg'
import DB from './FakeDB'
import "swiper/css"
import "swiper/css/free-mode"
import "swiper/css/navigation"
import "swiper/css/thumbs"
import { FreeMode, Navigation, Thumbs } from "swiper";
import Image from 'components/Image/Image'


const ProductReview = () => {

    const [thumbsSwiper, setThumbsSwiper] = useState<any>(null);

    const [thumbsSwiper1, setThumbsSwiper1] = useState<any>(null);
    const [thumbsSwiper2, setThumbsSwiper2] = useState<any>(null);
    const [thumbsSwiper3, setThumbsSwiper3] = useState<any>(null);

    let [isLoading,setIsLoading] = useState<Boolean>(false) 

    let [linkPreview,setLinkPreview] = useState<string>("")

    let [jsonVideo,setJsonVideo] = useState<any>([])

    let [keyVideo,setKeyVideo] = useState<string>("Product_Features") 

    let [isHiddenPreview,setIsHiddenPreview] = useState<Boolean>(false)

    const navigationPrevRef = React.useRef(null)
    const navigationNextRef = React.useRef(null)
    const handleOpenPreview = (link:string)=>{
        setIsHiddenPreview(true)
        setLinkPreview(link)
    }

    useEffect(()=>{
        setJsonVideo(DB[keyVideo])
    },[keyVideo])
    return (
    <div className={`${styles.productReview} container`}>
        <div className={`${styles.groupBtn} item-btw w-30`}>
            <div className={`${styles.tab1} ${keyVideo === "Revit_Mep" ? "colorTab":""}`} onClick={()=> {
                setJsonVideo(DB["Revit_Mep"])
                setKeyVideo("Revit_Mep")
                }}>
                Addin For Revit Mep
            </div>
            <div className={`${styles.tab2} ${keyVideo === "Product_Features" ? "colorTab":""}`} onClick={()=>{
                setKeyVideo("Product_Features")
             setJsonVideo(DB["Product_Features"])}}>
                Sản phẩm tương lai
            </div>
        </div>
        <div className={`${styles.mainContent} container`}>
            <div className={`${styles.items}`}>
                <p className={styles.titleItem}> <i className="fa-solid fa-screwdriver-wrench"></i> Modelling Tools</p>
                
                <Swiper
                            loop={true}
                            spaceBetween={10}
                            navigation={true}
                            thumbs={{swiper: thumbsSwiper && !thumbsSwiper?.destroyed ? thumbsSwiper : null}}
                            modules={[FreeMode, Navigation, Thumbs]}
                            className="mySwiperProduct"
                        >
                        { jsonVideo.length ===0 ? <LoaderItem/> :  jsonVideo[`${keyVideo}1`].map((value:any,index:number) => {
                            return <SwiperSlide>
                                <div className={`${styles.mainSlide} item-btw w-100`}>
                                    <div className={`${styles.cart} w-100 item-btw`} onClick={()=>handleOpenPreview(value.link)} >
                                            
                                            <div className={`${styles.mainPost} w-50`}>
                                                <p className={styles.title1}>{value.title}</p>
                                                <div></div>
                                                <p  className={styles.content}>{ GetWinDown() == 1 ?   value.content.slice(0,200) : value.content+" ..."}</p>
                                            </div>
                                            <div className={`${styles.image} w-50`}>
                                                <ReactPlayer
                                                    width={"100%"}
                                                    height={400}
                                                            url={value.link}
                                                        />
                                            </div>
                                    </div>
                                </div>
                            </SwiperSlide>
                        })}
                    </Swiper>  
                    <Swiper
                             onSwiper={setThumbsSwiper}
                             loop={true}
                             spaceBetween={3}
                             slidesPerView={GetWinDown() == 1 ? 2 :4}
                             freeMode={true}
                             watchSlidesProgress={true}
                             modules={[FreeMode, Navigation, Thumbs]}
                        >
                        { jsonVideo.length ===0 ? <LoaderItem/> :  jsonVideo[`${keyVideo}1`].map((value:any,index:number) => {
                            return <SwiperSlide>
                                <div className={`${styles.mainSlide} item-btw w-100`}>
                                    <div className={styles.cart1} >
                                        <div className={styles.mainPost1}>
                                           <div className={styles.icons}>
                                                <Image image={ToolIcons} contain={"contain"}/>
                                           </div>
                                            <div>
                                                <p className={styles.title1}>{value.title}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </SwiperSlide>
                        })}
                    </Swiper>  
            </div>
            <div className={`${styles.items} ${styles.items1}`}>
                <p className={styles.titleItem}><i className="fa-solid fa-draw-polygon"></i> Drawing Tools</p>
                <Swiper
                            loop={true}
                            spaceBetween={10}
                            navigation={true}
                            thumbs={{swiper: thumbsSwiper1 && !thumbsSwiper1?.destroyed ? thumbsSwiper1 : null}}
                            modules={[FreeMode, Navigation, Thumbs]}
                        >
                        {  jsonVideo.length ===0  ? <LoaderItem/> :   jsonVideo[`${keyVideo}2`].map((value:any,index:number) => {
                           return <SwiperSlide>
                           <div className={`${styles.mainSlide} item-btw w-100`}>
                               <div className={`${styles.cart} w-100 item-btw`} onClick={()=>handleOpenPreview(value.link)} >
                                       
                                       <div className={`${styles.mainPost} w-50`}>
                                           <p className={styles.title1}>{value.title}</p>
                                           <div></div>
                                           <p  className={styles.content}>{value.content}</p>
                                       </div>
                                       <div className={`${styles.image} w-50`}>
                                           <ReactPlayer
                                               width={"100%"}
                                               height={400}
                                                       url={value.link}
                                                   />
                                       </div>
                               </div>
                           </div>
                       </SwiperSlide>
                        })}
                    </Swiper>  
                    <Swiper
                             onSwiper={setThumbsSwiper1}
                             loop={true}
                             spaceBetween={3}
                             slidesPerView={4}
                             freeMode={true}
                             watchSlidesProgress={true}
                             modules={[FreeMode, Navigation, Thumbs]}
                        >
                        { jsonVideo.length ===0 ? <LoaderItem/> :  jsonVideo[`${keyVideo}2`].map((value:any,index:number) => {
                            return <SwiperSlide>
                                <div className={`${styles.mainSlide} item-btw w-100`}>
                                    <div className={styles.cart1} >
                                        <div className={styles.mainPost1}>
                                           <div className={styles.icons}>
                                                <Image image={ToolIcons} contain={"contain"}/>
                                           </div>
                                            <div>
                                                <p className={styles.title1}>{value.title}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </SwiperSlide>
                        })}
                    </Swiper>  

            </div>
            <div className={`${styles.items} ${styles.items1}`}>
                <p className={styles.titleItem}><i className="fa-solid fa-faucet-drip"></i>  Quantification Tools</p>
                <Swiper
                            loop={true}
                            spaceBetween={10}
                            navigation={true}
                            thumbs={{swiper: thumbsSwiper2 && !thumbsSwiper2?.destroyed ? thumbsSwiper2 : null}}
                            modules={[FreeMode, Navigation, Thumbs]}
                        >
                        {  jsonVideo.length ===0 ? <LoaderItem/> :   jsonVideo[`${keyVideo}3`].map((value:any,index:number) => {
                            return <SwiperSlide>
                            <div className={`${styles.mainSlide} item-btw w-100`}>
                                <div className={`${styles.cart} w-100 item-btw`} onClick={()=>handleOpenPreview(value.link)} >
                                        
                                        <div className={`${styles.mainPost} w-50`}>
                                            <p className={styles.title1}>{value.title}</p>
                                            <div></div>
                                            <p  className={styles.content}>{value.content}</p>
                                        </div>
                                        <div className={`${styles.image} w-50`}>
                                            <ReactPlayer
                                                width={"100%"}
                                                height={400}
                                                        url={value.link}
                                                    />
                                        </div>
                                </div>
                            </div>
                        </SwiperSlide>
                        })}
                    </Swiper>  
                    <Swiper
                             onSwiper={setThumbsSwiper2}
                             loop={true}
                             spaceBetween={3}
                             slidesPerView={4}
                             freeMode={true}
                             watchSlidesProgress={true}
                             modules={[FreeMode, Navigation, Thumbs]}
                        >
                        { jsonVideo.length ===0 ? <LoaderItem/> :  jsonVideo[`${keyVideo}3`].map((value:any,index:number) => {
                            return <SwiperSlide>
                                <div className={`${styles.mainSlide} item-btw w-100`}>
                                    <div className={styles.cart1} >
                                        <div className={styles.mainPost1}>
                                           <div className={styles.icons}>
                                                <Image image={ToolIcons} contain={"contain"}/>
                                           </div>
                                            <div>
                                                <p className={styles.title1}>{value.title}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </SwiperSlide>
                        })}
                    </Swiper>  

            </div>
            <div className={`${styles.items} ${styles.items1}`}>
                <p className={styles.titleItem}><i className="fa-solid fa-paragraph"></i>  Parameter Tools</p>
                <Swiper
                             loop={true}
                             spaceBetween={10}
                             navigation={true}
                             thumbs={{swiper: thumbsSwiper3 && !thumbsSwiper3?.destroyed ? thumbsSwiper3 : null}}
                             modules={[FreeMode, Navigation, Thumbs]}
                        >
                        {  jsonVideo.length ===0  ? <LoaderItem/> :   jsonVideo[`${keyVideo}4`].map((value:any,index:number) => {
                            return <SwiperSlide>
                            <div className={`${styles.mainSlide} item-btw w-100`}>
                                <div className={`${styles.cart} w-100 item-btw`} onClick={()=>handleOpenPreview(value.link)} >
                                        
                                        <div className={`${styles.mainPost} w-50`}>
                                            <p className={styles.title1}>{value.title}</p>
                                            <div></div>
                                            <p  className={styles.content}>{value.content}</p>
                                        </div>
                                        <div className={`${styles.image} w-50`}>
                                            <ReactPlayer
                                                width={"100%"}
                                                height={400}
                                                        url={value.link}
                                                    />
                                        </div>
                                </div>
                            </div>
                        </SwiperSlide>
                        })}
                    </Swiper>  
                    <Swiper
                             onSwiper={setThumbsSwiper3}
                             loop={true}
                             spaceBetween={3}
                             slidesPerView={4}
                             freeMode={true}
                             watchSlidesProgress={true}
                             modules={[FreeMode, Navigation, Thumbs]}
                        >
                        { jsonVideo.length ===0 ? <LoaderItem/> :  jsonVideo[`${keyVideo}4`].map((value:any,index:number) => {
                            return <SwiperSlide>
                                <div className={`${styles.mainSlide} item-btw w-100`}>
                                    <div className={styles.cart1} >
                                        <div className={styles.mainPost1}>
                                           <div className={styles.icons}>
                                                <Image image={ToolIcons} contain={"contain"}/>
                                           </div>
                                            <div>
                                                <p className={styles.title1}>{value.title}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </SwiperSlide>
                        })}
                    </Swiper> 
            </div>
                            
        </div>
        {isHiddenPreview ? <Preview handleClose={()=>setIsHiddenPreview(false)} link = {linkPreview}/>:<></>}
    </div>
  )
}

export default ProductReview