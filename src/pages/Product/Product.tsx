import BreakCrumb from 'components/BreakCrumb/BreakCrumb'
import ComponentPage from 'components/ComponentPage/ComponentPages'
import  { useState } from 'react'
import styles from './styles/product.module.scss'
import ProductMain from './components/ProductMain/ProductMain'
import { MetaSeo } from 'constant/Constant'
const Product = () => {
  
  let [stringBreak,setStringBreak] = useState("Trang Chủ / Sản Phẩm")

  let handleContent = (string:string)=>{
    setStringBreak(string)
  }
  return (

    <div className={styles.product}>
          <ComponentPage isHiddenHeader={true} isHiddenFooter={true} seo = {MetaSeo.ProductPage}>
            <BreakCrumb content = {stringBreak}/>
            <ProductMain/>
          </ComponentPage>
      </div>
  )
}

export default Product