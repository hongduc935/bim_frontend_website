import styles from './styles/cart.module.scss'
import PaymentAPI from 'api/payment.api'


const Cart = () => {

    const handlePayment =async ()=>{
        let result_payment:any = await PaymentAPI.createPayment({})
        console.log(result_payment)
        if(result_payment?.statusCode === 1000){
            // window.location.href = result_payment?.data
            window.open(result_payment?.data, '_blank')?.focus()
        }
    }

    return (
            <div className={`${styles.cart} item-center container`}>
                <p className={styles.titleCart}>Thông tin đơn hàng</p>
                <div className={styles.cartMain}>
                    <p>Quản lí đơn hàng</p>
                    <div className={styles.table}>

                    <table className={styles.table_1}>
                        
                        <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>Tên đơn hàng</th>
                            <th>Thời hạn</th>
                            <th>Số lượng</th>
                            <th>Đơn giá</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Linsce Key</td>
                            <td>Combo 5</td>
                            <td>60 ngày</td>
                            <td>1</td>
                            <td>900000 VND</td>
                        </tr>
                    </tbody>
                    <caption>Products purchased last month</caption>
                    </table>   
                    <button className={styles.payment} onClick={handlePayment}>Thanh Toán đơn hàng</button> 
                    </div>
                </div>
            </div>
    )
}

export default Cart