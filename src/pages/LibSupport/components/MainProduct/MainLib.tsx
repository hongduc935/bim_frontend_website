import { useEffect, useState } from 'react'
import styles from './styles/main-product.module.scss'
import "swiper/css"
import "swiper/css/pagination"

import DownloadDB from './DataFake/DownloadDB'
export interface IMakeDB {
    title:string
    link_totorial:string
    description:string
}

const MainLib = (props:any) => {

    let [stringBreak,setStringBreak] = useState("Thư Viện Hỗ Trợ")

    let [keyDB ,setKeyDB] = useState<String>("lib_mep/lib_mep_project")

    let [linkDownLoad,setLinkDownload] = useState<any[]>([])
    let [linkVideo,setLinkVideo] = useState<any[]>([])

    useEffect(()=>{
        let list_key:string[] = keyDB.split("/")
        if(list_key.length > 0 ){
            setLinkVideo(DownloadDB[`${list_key[0]}`][`${list_key[1]}`][`link_video`])
            setLinkDownload(DownloadDB[`${list_key[0]}`][`${list_key[1]}`][`link_download`])
        }
      
    },[keyDB,linkVideo,linkDownLoad])

    return (
        <div className={`${styles.mainProduct}  item-center`}>
            <div className={styles.left}>
                <div className={styles.top}>
                    <div className={styles.title}> <i className="fa-solid fa-grid-2 m-r-5"></i> 1. THƯ VIỆN REVIT MEP</div>
                    <div className={styles.name} onClick={()=> {
                        props?.handleContent("Trang Chủ / 1. THƯ VIỆN REVIT MEP / Dự án mẫu"); 
                        setStringBreak("Dự án mẫu");
                        setKeyDB("lib_mep/lib_mep_project")
                    } }>Dự án mẫu</div>
                    <div className={styles.name} onClick={()=>{ 
                        props?.handleContent("Trang Chủ / 1. THƯ VIỆN REVIT MEP / Thư viện valve");
                        setStringBreak("Thư viện valve")
                        setKeyDB("lib_mep/lib_mep_libvalve")
                    }}>Thư viện valve</div>
                    <div className={styles.name} onClick={()=> {
                        props?.handleContent("Trang Chủ / 1. THƯ VIỆN REVIT MEP / Thư viện Fitting ống nước");
                        setStringBreak("Thư viện Fitting ống nước")
                        setKeyDB("lib_mep/lib_mep_libwater")
                         } }>Thư viện Fitting ống nước</div>
                    <div className={styles.name} onClick={()=>{ 
                        props?.handleContent("Trang Chủ / 1. THƯ VIỆN REVIT MEP / Thư viện Fitting ống gió"); 
                        setStringBreak("Thư viện Fitting ống gió")
                        setKeyDB("lib_mep/lib_mep_libwin")
                        }}>Thư viện Fitting ống gió</div>
                </div>
                <div className={`${styles.top} ${styles.top1}`}>
                    <div className={styles.title}><i className="fa-solid fa-grid-2 m-r-5"></i> 2. THƯ VIỆN REVIT API</div>
                    <div className={styles.name} onClick={()=>{ 
                        props?.handleContent("Trang Chủ / 2. THƯ VIỆN REVIT API / Các Method");
                        setStringBreak("Các Method")
                        setKeyDB("lib_revit/lib_revit_method")
                         }}> Các Method</div>
                    <div className={styles.name} onClick={()=>{ 
                        props?.handleContent("Trang Chủ / 2. THƯ VIỆN REVIT API / Bài tập mẫu");
                         setStringBreak("Bài tập mẫu")
                         setKeyDB("lib_revit/lib_revit_sample")
                         }}>Bài tập mẫu</div>
                </div>
                <div className={`${styles.top} ${styles.top1}`}>
                    <div className={styles.title}><i className="fa-solid fa-grid-2 m-r-5"></i> 3. HQL TOOLS</div>
                    <div className={styles.name} onClick={()=>{
                         props?.handleContent("Trang Chủ / 3. HqlTools / Các Phiên Bản");
                          setStringBreak("Các Phiên Bản");
                          setKeyDB("lib_tools/lib_tools_version")
                          }}>Các Phiên Bản</div>
                    <div className={styles.name} onClick={()=>{ 
                        props?.handleContent("Trang Chủ / 3. HqlTools / Hướng Dẫn Cài Đặt"); 
                        setStringBreak("Hướng Dẫn Cài Đặt");
                        setKeyDB("lib_tools/lib_tools_setting")
                        }}>Hướng Dẫn Cài Đặt</div>
                </div>
            </div>
            <div className={styles.right}>
                    <div className={styles.title}><i className="fa-solid fa-grid-2 m-r-5"></i>{stringBreak} </div>
                    <div className={styles.grid}>
                        <div className={styles.items}>
                            <p className={styles.titleDOwn}>Link tài liệu hướng dẫn</p>
                            <ul  className={styles.listvd}>
                                {
                                    linkDownLoad?.length > 0 ?  linkDownLoad.flatMap((value:string,index:number)=>{
                                        return <li key={`donwload${index}`}> <a href={value}>{value} </a> </li>
                                    }):<></>
                                } 
                            </ul>
                            
                            <p className={styles.titleDOwn}>Link video hướng dẫn</p>
                                <ul className={styles.listvd}>
                                    {
                                        setLinkVideo?.length > 0 ?  linkVideo.flatMap((value:string,index:number)=>{
                                            return <li key={`donwload${index}`}> <a href={value}>{value} </a> </li>
                                        }):<></>
                                    }
                                </ul>
                        </div>
                    </div>
            </div>
        </div>
    )
}

export default MainLib