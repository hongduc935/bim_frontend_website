

const MakeDB1 = [

    {
        link_totorial:"https://www.youtube.com/watch?v=shLUsd7kQCI&list=RDshLUsd7kQCI&start_radio=1",
        title:"Hướng dẫn học hát",
        description:"Kiến thức cần thiết để xây dựng 1 project mẫu revit api"
    },
    {
        link_totorial:"https://www.youtube.com/watch?v=shLUsd7kQCI&list=RDshLUsd7kQCI&start_radio=1",
        title:"Hướng dẫn học hát 2",
        description:"Kiến thức cần thiết để xây dựng 1 project mẫu revit api addin"
    },

]

const MakeDB:any = {
    "lib_mep":{
        "lib_mep_project":[
            {
                link_totorial:"https://www.youtube.com/watch?v=shLUsd7kQCI&list=RDshLUsd7kQCI&start_radio=1",
                title:"Hướng dẫn học hát",
                description:"Kiến thức cần thiết để xây dựng 1 project mẫu revit api"
            },
            {
                link_totorial:"https://www.youtube.com/watch?v=shLUsd7kQCI&list=RDshLUsd7kQCI&start_radio=1",
                title:"Hướng dẫn học hát 2",
                description:"Kiến thức cần thiết để xây dựng 1 project mẫu revit api addin"
            },
            {
                link_totorial:"https://www.youtube.com/watch?v=shLUsd7kQCI&list=RDshLUsd7kQCI&start_radio=1",
                title:"Hướng dẫn học hát 2",
                description:"Kiến thức cần thiết để xây dựng 1 project mẫu revit api addin"
            },
        ],
        "lib_mep_libvalve":[
            {
                link_totorial:"https://www.youtube.com/watch?v=JHSRTU31T14&list=RDJHSRTU31T14&index=1",
                title:"Hướng dẫn học hát",
                description:"Kiến thức cần thiết để xây dựng 1 project mẫu revit api"
            },
            {
                link_totorial:"https://www.youtube.com/watch?v=DKs6nOcl5Ws",
                title:"Hướng dẫn học hát 2",
                description:"Kiến thức cần thiết để xây dựng 1 project mẫu revit api addin"
            },
        ],
        "lib_mep_libwater":[
            {
                link_totorial:"https://www.youtube.com/watch?v=13WEkgvCs0Q",
                title:"Hướng dẫn học hát",
                description:"Kiến thức cần thiết để xây dựng 1 project mẫu revit api"
            },
            {
                link_totorial:"https://www.youtube.com/watch?v=gQVWGj3h1pM",
                title:"Hướng dẫn học hát 2",
                description:"Kiến thức cần thiết để xây dựng 1 project mẫu revit api addin"
            },
        ],
        "lib_mep_libwin":[
            {
                link_totorial:"https://www.youtube.com/watch?v=HSknuSIoK6A",
                title:"Hướng dẫn học hát",
                description:"Kiến thức cần thiết để xây dựng 1 project mẫu revit api"
            },
            {
                link_totorial:"https://www.youtube.com/watch?v=Bs41dR_Kf-0",
                title:"Hướng dẫn học hát 2",
                description:"Kiến thức cần thiết để xây dựng 1 project mẫu revit api addin"
            },
        ]
    },
    "lib_revit":{
        "lib_revit_method":[
            {
                link_totorial:"https://www.youtube.com/watch?v=shLUsd7kQCI&list=RDshLUsd7kQCI&start_radio=1",
                title:"Hướng dẫn học hát",
                description:"Kiến thức cần thiết để xây dựng 1 project mẫu revit api"
            },
            {
                link_totorial:"https://www.youtube.com/watch?v=shLUsd7kQCI&list=RDshLUsd7kQCI&start_radio=1",
                title:"Hướng dẫn học hát 2",
                description:"Kiến thức cần thiết để xây dựng 1 project mẫu revit api addin"
            },
        ],
        "lib_revit_sample":[
            {
                link_totorial:"https://www.youtube.com/watch?v=shLUsd7kQCI&list=RDshLUsd7kQCI&start_radio=1",
                title:"Hướng dẫn học hát",
                description:"Kiến thức cần thiết để xây dựng 1 project mẫu revit api"
            },
            {
                link_totorial:"https://www.youtube.com/watch?v=shLUsd7kQCI&list=RDshLUsd7kQCI&start_radio=1",
                title:"Hướng dẫn học hát 2",
                description:"Kiến thức cần thiết để xây dựng 1 project mẫu revit api addin"
            },
        ]
    },
    "lib_tools":{
        "lib_tools_version":[
            {
                link_totorial:"https://www.youtube.com/watch?v=shLUsd7kQCI&list=RDshLUsd7kQCI&start_radio=1",
                title:"Hướng dẫn học hát",
                description:"Kiến thức cần thiết để xây dựng 1 project mẫu revit api"
            },
            {
                link_totorial:"https://www.youtube.com/watch?v=shLUsd7kQCI&list=RDshLUsd7kQCI&start_radio=1",
                title:"Hướng dẫn học hát 2",
                description:"Kiến thức cần thiết để xây dựng 1 project mẫu revit api addin"
            },
        ],
        "lib_tools_setting":[
            {
                link_totorial:"https://www.youtube.com/watch?v=shLUsd7kQCI&list=RDshLUsd7kQCI&start_radio=1",
                title:"Hướng dẫn học hát",
                description:"Kiến thức cần thiết để xây dựng 1 project mẫu revit api"
            },
            {
                link_totorial:"https://www.youtube.com/watch?v=shLUsd7kQCI&list=RDshLUsd7kQCI&start_radio=1",
                title:"Hướng dẫn học hát 2",
                description:"Kiến thức cần thiết để xây dựng 1 project mẫu revit api addin"
            },
        ]
    }
}

export default MakeDB