import React, { useState } from 'react'
import styles from './styles/lib-support.module.scss'
import ComponentPage from 'components/ComponentPage/ComponentPages'
import BreakCrumb from 'components/BreakCrumb/BreakCrumb'
import MainLib from './components/MainProduct/MainLib'
const LibSupport = () => {

  let [stringBreak,setStringBreak] = useState("Trang Chủ / Thư Viện Hỗ Trợ")

  let handleContent = (string:string)=>{
    setStringBreak(string)
  }
  return (
    <div className={styles.product}>
        <ComponentPage isHiddenHeader={true} isHiddenFooter={true}>
           <BreakCrumb content = {stringBreak}/>
           <MainLib handleContent={handleContent} />
        </ComponentPage>

    </div>
  )
}

export default LibSupport