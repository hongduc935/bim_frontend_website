import styles from './styles/send-message.module.scss'
import { useForm, SubmitHandler } from "react-hook-form"
import FeedbackAPI from 'api/feedback.api'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
type Inputs = {
  name: string,
  email: string,
  content:string
};
const SendMessage = () => {
    const { register, handleSubmit, watch, formState: { errors } } = useForm<Inputs>();
    const notify = (content:string ) => toast(`${content}`!)
    const onSubmit: SubmitHandler<Inputs> =async (data) => {
        try {
            let response1:any = await FeedbackAPI.createFeedback(data)
            if(response1?.statusCode === 2000 ){
                notify(response1?.msg)
            }
            else{
                notify(response1?.msg)
            }
        } catch (error) {
            notify("Server đang bảo trì !")
        }
    };
    return (
    <div className={`${styles.sendMessage} container`}>
        <div className={styles.nav}>
             <h2>Gửi tin nhắn</h2>
        </div>
        <div className={`${styles.mainSend} item-btw w-100`}>
            <div className={`${styles.form} w-50`}>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className={styles.formField}>
                        <input  {...register("name")} placeholder='Tên' />
                    </div>
                    <div className={styles.formField}>
                        <input {...register("email", { required: true })} placeholder='Email'  />
                    </div>
                    <div className={styles.formField}>
                        <textarea {...register("content", { required: true })} placeholder='Message'  />
                    </div>
                    <div className={styles.formField1}>
                        <input  placeholder='Message' type='checkbox'  />Tôi đã đọc và đồng ý với <strong> Điều khoản và điều kiện</strong>
                    </div>
                    <div className={styles.buttonForm}>
                        <button type='submit'>Gửi</button>
                    </div>
                </form>
            </div>
            <div className={`${styles.info} w-40`}>
                <p> <i className="fa-solid fa-phone"></i>  &nbsp;0987 904 586</p>
                <p> <i className="fa-solid fa-envelope"></i>  &nbsp;bimsmart.hql@gmail.com</p>
                <p><i className="fa-sharp fa-solid fa-location-dot"></i> &nbsp; 77 Võ Văn Kiệt, phường An Lạc, Quận Bình Tân, Thành phố Hồ Chí Minh</p>
                <div className={styles.listIc}>
                    <div className={styles.icon}>
                        <i className="fa-brands fa-facebook"></i>
                    </div>
                    <div className={styles.icon}>
                        <i className="fa-brands fa-twitter"></i>
                    </div> <div className={styles.icon}>
                        <i className="fa-brands fa-instagram"></i>
                    </div>
                    <div className={styles.icon}>
                        <i className="fa-brands fa-linkedin"></i>
                    </div>
                    <div className={styles.icon}>
                        <i className="fa-brands fa-telegram"></i>
                    </div>
                    <div className={styles.icon}>
                        <i className="fa-brands fa-youtube"></i>
                    </div>

                </div>
            </div>
        </div>
        <ToastContainer/>
    </div>
  )
}

export default SendMessage