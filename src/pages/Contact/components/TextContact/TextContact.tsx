import styles from './styles/text-contact.module.scss'

const TextContact = () => {
  return (
    <div className={`${styles.textContact} container`}>
        <p className={styles.title}>Liên Hệ</p>
        <ul className={styles.menu}>
            <li>Cảm ơn Quý Khách hàng đã quan tâm đến Sản phẩm và Dịch vụ của HQL - Revit Mep Plugins.</li>
            <li>Chúng tôi luôn sẵn sàng tư vấn và giải đáp thắc mắc của quý khách.</li>
            <li>Vui lòng để lại thông tin, chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất.</li>
            <li>Hoặc gọi Hotline:&nbsp;<span className={styles.phone}>0987 904 586</span>  để được tư vấn.</li>
            <li className={styles.thank}>Trân trọng cảm ơn!</li>
        </ul>
        <p className={styles.title1}>Bên cạnh đó, Quý Khách hàng có thể tham khảo các tình huống: </p>
        <ul className={styles.menu2}>
            <li>
                <a>Hướng dẫn đăng ký tài khoản khách hàng.</a>
            </li>
            <li>
                <a>Hướng dẫn đăng ký Đại lý.</a>
            </li>
            <li>
                <a>Hướng dẫn thanh toán và thủ tục hoàn tiền.</a>
            </li>
            <li>
                <a>Hướng dẫn xử lý sự cố và khiếu nại.</a>
            </li>
            <li>
                <a>Hướng dẫn kích hoạt sản phẩm, dịch vụ.</a>
            </li>
            <li>
                <a>Năng lực của Chúng tôi. Năng lực của Chúng tôi. </a>
            </li>
        </ul>
    </div>
  )
}

export default TextContact