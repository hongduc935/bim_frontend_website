import styles from './styles/contact.module.scss'
import ComponentPage from 'components/ComponentPage/ComponentPages'
import Map from './components/Map/Map'
import BreakCrumb from 'components/BreakCrumb/BreakCrumb'
import TextContact from './components/TextContact/TextContact'
import SendMessage from './components/SendMessage/SendMessage'

const stringBreak = "Trang Chủ / Liên Hệ"

const Seo = {
  title:"Liên hệ với BimMep Solution",
  description:"Chuyên cung cấp các giải pháp xây dựng bản vẽ 1 cách tiện lợi"
}

const Contact = () => {
  return (
    <>
    <div className={styles.contact}>
      <ComponentPage isHiddenHeader={true} isHiddenFooter={true} seo ={Seo}>
            <BreakCrumb content = {stringBreak}/>
            <Map/>
            <TextContact/>
            <SendMessage/>
      </ComponentPage>
      </div>
    </>
  )
}

export default Contact