import {useCallback, useEffect, useState} from "react"
import styles from "./styles/register.module.scss"
import {SubmitHandler, useForm} from "react-hook-form"
import { IRegister} from "@type/AuthInterface"
import {useAppDispatch} from "redux/store"
import {RegisterAction} from "redux/action/Auth/Auth"
import {toast, ToastContainer} from "react-toastify"
import PackageService from "api/package.api"

const RegisterComponent=(props:any)=>{

    const dispatch = useAppDispatch();
  
    let [packages, setPackages] = useState<any>(null)
    const notify = (content:string ) => toast(`${content}`!);

    const { register , handleSubmit, formState: { errors } } = useForm<IRegister>();

    const _fetchPackage = useCallback(async () => {
       return  await PackageService.getAllPackage();
    }, [])

    useEffect(()=>{

        if(errors?.email || errors?.password)
        {
            notify(" Validate Form Fail !!!");
        }

    },[errors,packages])

    useEffect(()=>{

        _fetchPackage().then((res:any)=>{
            setPackages(res?.data)
        })

    },[])


    const onSubmit: SubmitHandler<IRegister> = async ( data:any,e:any) => {
        try {
            if(Number(data.packages) === -1){
                toast.error("Bạn vui lòng chọn gói đăng kí để đăng kí tài khoản.",{autoClose:1500})
            }
            else if(data.confirm_password !== data.password){
                toast.error("Confirm passoword is not match password.",{autoClose:1500})
            }
            else{
                await dispatch(RegisterAction(data))
            }
            
        }catch (e) {
            notify("Register Fail")
        }
    };
    return(

        <div className={styles.login}>
            <form onSubmit={handleSubmit(onSubmit)} >
                <div className={styles.loginForm}>
                    <div className={styles.loginFormTitle}>
                        <h2>Đăng kí tài khoản</h2>
                        <p>Bạn vui lòng điền các thông tin bên dưới</p>
                    </div>
                    <div className={styles.loginFormBody}>
                        <div>
                            <div className={styles.loginFormBodyItem}>
                                <input type={"text"} placeholder='Enter your name'
                                       {...register("name",{ required: true })}
                                />
                                <span>{errors?.name&& "Name is validate *"} </span>
                            </div>
                            <div className={styles.loginFormBodyItem}>
                                <input type={"password"} placeholder='Enter your password'
                                       {...register("password",{ required: true,minLength:6 })}
                                />
                                <span>{errors?.password&& "Password is validate *"} </span>
                            </div>
                            <div className={styles.loginFormBodyItem}>
                                <input type={"text"} placeholder='Enter your phone'
                                       {...register("phone",{ required: true,minLength:10,maxLength:11 })}
                                />
                                <span>{errors?.phone&& "Phone is validate *"} </span>
                            </div>
                        </div>
                        <div>
                            <div className={styles.loginFormBodyItem}>
                                <input type={"text"} placeholder='Enter your email'
                                       {...register("email",{required:true,pattern:/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/})}
                                />
                                <span>{errors?.email&& "Email is validate *"} </span>
                            </div>
                            <div className={styles.loginFormBodyItem}>
                                <input type={"password"} placeholder='Enter confirm password'
                                {...register("confirm_password",{ required: true,minLength:6 })}
                                />
                                <span>{errors?.password&& "Password is validate *"} </span>
                            </div>
                            <div className={styles.loginFormBodyItem}>
                                <select {...register("packages")}>
                                    <option value={-1}>Gói đăng kí</option>
                                    {packages && packages?.length > 0 ? packages.flatMap((value:any,index:number)=>{
                                        return  <option value={value?.date_package ? value?.date_package : 360} key={`package${index}`}>
                                                    {value?.name_package} ({value?.price_package} VND)
                                                </option>
                                    }) :<>
                                    </>}
                                   
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className={styles.loginFormBottom}>
                        <div className={styles.loginFormBottomAction}>
                            <button className={styles.btnRegister} > Register</button>
                        </div>
                        <div  className={styles.loginFormBottomRegisterOrLogin}>
                            <p>Don't must have account ?  <span onClick={props.RegisterToLogin}>Click to Login</span></p>
                        </div>
                    </div>
                </div>
            </form>
            <ToastContainer />
        </div>
    )
}

export  default  RegisterComponent;