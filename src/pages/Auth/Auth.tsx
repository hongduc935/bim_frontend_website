import React from 'react'
import ComponentPage from "components/ComponentPage/ComponentPages"
import styles from './styles/auth.module.scss'
import LoginComponent from "pages/Auth/components/Login/LoginComponent"
import RegisterComponent from "pages/Auth/components/Register/RegisterComponent"
import Image from "components/Image/Image"
import Engineering from 'asset/images/back-side-engineer-worker_1308-100738.png'
import { useCallback } from "react";
import Particles from "react-particles";
import type { Container, Engine } from "tsparticles-engine"
import { loadFull } from "tsparticles";
import PackageService from 'api/package.api'
const Auth=()=>{

    const [isLogin,setIsLogin]=React.useState("register")

    function HandleChangLoginRegister(){
        setIsLogin("register")
    }
    function HandleChangLoginForgot(){
        setIsLogin("forgot")
    }
    function HandleChangLoginToVerify(){
        setIsLogin("verify")
    }


    const particlesInit = useCallback(async (engine: Engine) => {
        await loadFull(engine);
    }, []);

   

    const particlesLoaded = useCallback(async (container: Container | undefined) => {
        await console.log(container);
    }, []);

    return(
        <ComponentPage isHiddenHeader={true} isHiddenFooter={true}>
            <div className={`${styles.loginContainer} item-center`}>
                <div className={`${styles.loginMain}`}>
                    <div className={styles.img}>
                     
                        <div className={`${styles.roundMap} w-100 item-center`}>
                            <div className={`${styles.engineer} w-50`}>
                                <Image image={Engineering} contain='contain'/>
                            </div>
                            <div className={`${styles.round} w-50`}>
                                    <div className = {`${styles.purpose} ${styles.purpose1} item-start`}>
                                        <div className={`${styles.number} item-center`}>
                                            <p><i className="fa-sharp fa-solid fa-circle-nodes"></i></p>
                                        </div>
                                        <div className={`${styles.name}`}>
                                            Giải pháp nâng cao
                                        </div>
                                    </div>
                                    <div className = {`${styles.purpose} ${styles.purpose2} item-start`}>
                                        <div className={`${styles.number} item-center`}>
                                            <p><i className="fa-solid fa-cloud"></i></p>
                                        </div>
                                        <div className={`${styles.name}`}>
                                            Tiếp cận công nghệ
                                        </div>
                                    </div>
                                    <div className = {`${styles.purpose} ${styles.purpose3} item-start`}>
                                        <div className={`${styles.number} item-center`}>
                                            <p><i className="fa-sharp fa-solid fa-globe"></i></p>
                                        </div>
                                        <div className={`${styles.name}`}>
                                            Đón đầu xu hướng mới
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div className={styles.content}>
                        {isLogin==="login" && <LoginComponent ChangLoginToRegister={HandleChangLoginRegister}  HandleChangLoginForgot={HandleChangLoginForgot}  />}
                        {isLogin==="register" && <RegisterComponent RegisterToLogin={HandleChangLoginToVerify}
                         />}
                    </div>
                </div>
                <div className={styles.background}>
                <Particles
                className={styles.bgLogin}
            id="tsparticles"
            
            init={particlesInit}
            loaded={particlesLoaded}
            options={{
                background: {
                    color: {
                        value: "#ffffff",
                    },
                },
                fpsLimit: 4000,
                interactivity: {
                    events: {
                        onClick: {
                            enable: true,
                            mode: "push",
                        },
                        onHover: {
                            enable: true,
                            mode: "repulse",
                        },
                        resize: true,
                    },
                    modes: {
                        push: {
                            quantity: 4,
                        },
                        repulse: {
                            distance: 200,
                            duration: 0.4,
                        },
                    },
                },
                particles: {
                    color: {
                        value: "#3189a1",
                    },
                    links: {
                        color: "#3189a1",
                        distance: 150,
                        enable: true,
                        opacity: 0.5,
                        width: 1,
                    },
                    collisions: {
                        enable: true,
                    },
                    move: {
                        direction: "none",
                        enable: true,
                        outModes: {
                            default: "bounce",
                        },
                        random: false,
                        speed: 6,
                        straight: false,
                    },
                    number: {
                        density: {
                            enable: true,
                            area: 800,
                        },
                        value: 80,
                    },
                    opacity: {
                        value: 0.5,
                    },
                    shape: {
                        type: "circle",
                    },
                    size: {
                        value: { min: 1, max: 5 },
                    },
                },
                detectRetina: true,
            }}
        />
                </div>
            
            </div>
        </ComponentPage>
    )
}
export default  Auth