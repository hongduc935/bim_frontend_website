import styles from './styles/home.module.scss'
import ComponentPage from "components/ComponentPage/ComponentPages"
import Banner from "./components/Banner/Banner"
import Service from "./components/Service/Service"
import Benefit from "./components/Benefit/Benefit"
import PostNew from "./components/PostNew/PostNew"
import Tutorial from "./components/Tutorial/Tutorial"
import { MetaSeo } from 'constant/Constant'

const Home=()=>{

    return (
        <article className={styles.mainHome}>
        <ComponentPage
            isHiddenHeader={true} 
            isHiddenFooter={true}
            isHiddenScroll = {true}
            isHiddenZalo = {true}
            seo = {MetaSeo.HomePage}
        >
                    <Banner/>
                    <Service/>
                    <Benefit/>
                    <Tutorial/>
                    <PostNew/>
        </ComponentPage>
        </article>
    )
}
export default  Home