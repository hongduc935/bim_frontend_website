import styles from './styles/benefit.module.scss'
const Benefit = () => {
  return (
    <section className={`${styles.benefit} container`}>
        <div className={styles.title}>
            <h2  title="Tận Hưởng Các Ưu Điểm của Revit MEP, Revit API và BIM MEP">Các lợi ích chúng tôi mang đến cho bạn</h2>
        </div>
        <div className={styles.note}>
            <p>Tận hưởng các lợi ích không giới hạn của <strong>Revit MEP, Revit API BIM MEP</strong> , và  <strong> BIM MEP</strong>  với dịch vụ của chúng tôi, được thiết kế để tối ưu hóa quy trình làm việc, giảm thiểu chi phí và nâng cao hiệu quả công việc của bạn.</p>
        </div>

        <div className={styles.line1}>
            <div className={styles.item}>
                <div className={styles.image}>
                    <i className="fa-sharp fa-solid fa-business-time"></i>
                </div>
                <div className={styles.maiContent}>
                    <h4 className={styles.title}>Tiết kiệm thời gian</h4>
                    <p className={styles.content}>Bạn sẽ tiết kiệm được nhiều thời gian khi dùng Sản phẩm và dịch vụ của chúng tôi</p>
                </div>
            </div>

            <div className={styles.item}>
                <div className={styles.image}>
                     <i className="fa-sharp fa-solid fa-sack-dollar"></i>
                </div>
                <div className={styles.maiContent}>
                    <h4 className={styles.title}>Chi Phí</h4>
                    <p className={styles.content}>Bạn có thể yên tâm sử dụng dịch vụ của Chúng tôi chỉ từ 90.000/tháng.</p>
                </div>
            </div>
            <div className={styles.item}>
                <div className={styles.image}>
                    <i className="fa-sharp fa-solid fa-cloud-arrow-down"></i>
                </div>
                <div className={styles.maiContent}>
                    <h4 className={styles.title}>Cập nhật tự động</h4>
                    <p className={styles.content}>Các sản phẩm tự động cập nhật khi có kết nối Internet</p>
                </div>
            </div>
        </div>
        <div className={styles.line1}>
            <div className={styles.item}>
                <div className={styles.image}>
                <i className="fa-sharp fa-solid fa-credit-card"></i>
                </div>
                <div className={styles.maiContent}>
                    <h4 className={styles.title}>Thanh toán linh hoạt</h4>
                    <p className={styles.content}>Bạn chỉ cần ngồi tại chỗ và thanh toán các sản phẩm với rất nhiều tùy chọn thanh toán.</p>
                </div>
            </div>

            <div className={styles.item}>
                <div className={styles.image}>
                     <i className="fa-sharp fa-solid fa-handshake-angle"></i>
                </div>
                <div className={styles.maiContent}>
                    <h4 className={styles.title}>Đối tác tin cây</h4>
                    <p className={styles.content}>Nhiều chính sách hấp dân khi bạn trở thành nhà phân phối.</p>
                </div>
            </div>
            <div className={styles.item}>
                <div className={styles.image}>
                    <i className="fa-sharp fa-solid fa-handshake-angle"></i>
                </div>
                <div className={styles.maiContent}>
                    <h4 className={styles.title}>Hỗ trợ 24/7</h4>
                    <p className={styles.content}>Chúng tôi luôn cố gắng để lắng nghe bạn mỗi khi bạn gặp khó khăn khi sử dụng dịch vụ.</p>
                </div>
            </div>
        </div>
    </section>
  )
}

export default Benefit