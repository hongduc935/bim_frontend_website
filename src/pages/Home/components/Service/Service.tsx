import { useState } from 'react'
import Image from 'components/Image/Image'
import TrainingLogo from 'asset/images/Logo_Bim.png'
import Training1 from 'asset/images/TrainingAPI.png'
import Training2 from 'asset/images/TrainingRevit.jpg'
import styles from './styles//service.module.scss'
import MyPdfViewer from '../../../../components/PdfViewerComponent/PdfViewerComponent'
const content1:string ="HqlTools là một bộ công cụ tự động hóa trong Revit Mep, được thiết kế để tăng hiệu suất làm việc cho các Kỹ sư Bim Mep. Nó cung cấp các tính năng và thay thế việc lặp đi lặp lại như: Pick line cad, Đặt family tự động, chia ống, Kết nối thiết bị vệ sinh….Với bộ tool này giúp cho công ty giảm được thời gian và nhân sự. Giúp tăng năng suất, tiết kiệm chi phí ,chất lượng của công việc."
const content2:string = "Nếu công ty bạn đang dùng Bim mà vẫn thủ công công giai đoạn thiết kế, triển khai mô hình 3D thì hãy đến với chúng tôi. Chúng tôi sẽ giúp bạn tiết kiệm thời gian và nhân lực, chi phí bằng cách xây dựng cho cty bạn bộ tool riêng mang phong cách của công ty "
const content3:string = "Nếu bạn là một kỹ sư Bim Mep mà muốn có một công việc với mức lương cao thì bạn phải có sự khác biệt so với những kỹ sư khác. Muốn có sự khác biệt đó thì hãy đến với chúng tôi bằng cách tham gia lớp lập trình Revit API chuyên viết về MEP, khi đó bạn sẽ tự động tạo ra sản phẩm cho riêng mình và cho cty.  "
const content4:string = "Theo quyết định của thủ tướng chính phủ về áp dụng Bim cho ngành xây dựng. Nếu bạn là một kỹ sư Bim Mep mà không biết gì về công cụ Revit Mep  để làm Bim thì quả là sai lầm. Hãy đến với chúng tôi tham gia ngay lớp chuyên dạy về revit Mep. "
const Service = () => {


    let [isMore1,setIsMore1] = useState(true)
    let [isMore2,setIsMore2] = useState(true)
    let [isMore3,setIsMore3] = useState(true)
    let [isMore4,setIsMore4] = useState(true)
 
    let [openPDF,setOpenPDF] = useState(false)


    const handleClose = ()=>{
        setOpenPDF(false)
    }

    return (
        <section className={styles.service}>
            <div className={styles.title}>
                <h2>Sản phẩm và Dịch vụ</h2>
            </div>
            <div className={`${styles.note} container`}>
            <p>Chào mừng bạn đến với giải pháp hàng đầu trong việc <strong>tự động hóa</strong> và <strong>đào tạo add-in cho Revit MEP</strong>, dành riêng cho cá nhân và doanh nghiệp trong lĩnh vực <strong>BIM</strong>. Hãy để chúng tôi giúp bạn nâng cao hiệu quả công việc và <strong>tối ưu hóa các quy trình thiết kế MEP</strong> với công cụ chuyên biệt của chúng tôi. Từ việc đào tạo cơ bản đến nâng cao, chúng tôi cung cấp mọi thứ bạn cần để biến kiến thức <strong>Revit MEP</strong> thành lợi thế cạnh tranh trong ngành.</p>

            </div>
            <div className={`${styles.items} `}>
                <div className={`${styles.listItem} `}>
                    <div className={`${styles.item} ${styles.bg1} `}>
                        <div className={styles.imageIC}>
                            {/* <i className="fa-brands fa-microsoft"></i> */}
                            <Image image={TrainingLogo} contain='cover'/>
                        </div>
                        <div className={styles.titleItem}>
                            <h3 title='HQL TOOLS'>HqlTools</h3>
                        </div>
                        <div className={styles.content}>
                            <p>{content1.length > 200 && isMore1 ? content1.slice(0,200) + "..." :content1}</p>
                        </div>
                        <div className={styles.btnShow} onClick={()=>setIsMore1(!isMore1)}>{isMore1 ? "Show more" : "Show Less"} <i className="fa-sharp fa-solid fa-caret-down"></i></div>
                    </div>
                </div>
                <div className={styles.listItem}>
                    <div className={`${styles.item} ${styles.bg2}`}>
                        <div className={styles.imageIC}>
                            <Image image={"https://bimdev.vn/wp-content/uploads/2020/02/revit-addin-steps-theme-1-e1581517115327.png"} contain='cover'/>
                        </div>
                        <div className={styles.titleItem}>
                            <h3 title='Xây  Dựng ADDIN'>Xây Dựng ADDIN</h3>
                        </div>
                        <div className={styles.content}>
                            <p><p>{content2.length > 200 && isMore2 ? content2.slice(0,190) + "..." :content2}</p></p>
                        </div>
                        <div onClick={()=>setIsMore2(!isMore2)}>{isMore2 ? "Show more" : "Show Less"} <i className="fa-sharp fa-solid fa-caret-down"></i></div>
                    </div>
                </div>
                <div className={styles.listItem}>
                    <div className={`${styles.item} ${styles.bg3}`}>
                        <div className={styles.imageIC}>
                            {/* <i className="fa-sharp fa-solid fa-briefcase"></i> */}
                            <Image image={Training1} contain='cover'/>
                        </div>
                        <div className={styles.titleItem}>
                            <h4 title='REVIT API'>Training REVIT API</h4>
                        </div>
                        <div className={styles.content}>
                            <p>{content3.length > 200 && isMore3 ? content3.slice(0,190) + "..." :content3}</p>
                        </div>
                        <div onClick={()=>setIsMore3(!isMore3)}>{isMore3 ? "Show more" : "Show Less"}  <i className="fa-sharp fa-solid fa-caret-down"></i></div>
                    </div>
                </div>
                <div className={styles.listItem}>
                    <div className={`${styles.item} ${styles.bg4}`} onClick={()=>setOpenPDF(true)}>
                        <div className={styles.imageIC}>
                            {/* <i className="fa-solid fa-lightbulb"></i> */}
                            <Image image={Training2} contain='cover'/>
                        </div>
                        <div className={styles.titleItem}>
                            <h4 title='REVIT MEP'>Training REVIT MEP</h4>
                        </div>
                        <div className={styles.content}>
                            <p>{content4.length > 200 && isMore4? content4.slice(0,190) + "..." :content4}</p>
                        </div>
                        <div onClick={()=>setIsMore4(!isMore4)}>{isMore4 ? "Show more" : "Show Less"}  <i className="fa-sharp fa-solid fa-caret-down"></i></div>
                    </div>
                </div>
            </div>
            {
                openPDF ?  <div className={styles.viewPdf}>

                <MyPdfViewer handleClose={handleClose} />
            </div> : <></>
            }
           
        </section>
  )
}

export default Service