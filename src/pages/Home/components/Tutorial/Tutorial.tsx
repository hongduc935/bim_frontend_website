import styles from './styles/tutorial.module.scss'
import "swiper/css"
import "swiper/css/free-mode"
import "swiper/css/pagination"
import ReactPlayer from "react-player"
import { Swiper, SwiperSlide } from "swiper/react"
import Preview from './component/Preview/Preview'
import SwiperCore, { Pagination, Autoplay, Navigation } from "swiper"
import { useState, useRef } from 'react'
import ListTutorialDB from './constants'
SwiperCore.use([Autoplay])

function GetWinDown() {
    const width = window.innerWidth;
    if (width < 768) {
        return 1;
    }
    else {
        return 3;
    }
}

const Tutorial = () => {
    const navigationPrevRef = useRef(null)
    const navigationNextRef = useRef(null)
    let [isHiddenPreview, setIsHiddenPreview] = useState<Boolean>(false)
    const handleOpenPreview = () => {
        setIsHiddenPreview(true)
    }
    return (
        <section className={styles.tutorial}>
            <div className={styles.title}>
                <h2>Sản Phẩm Nội Bật</h2>
            </div>
            <div className={styles.note}>
                <p>Khám phá toàn diện các tính năng và lợi ích của <strong>HQL - Revit MEP Plugin</strong>, công cụ đắc lực giúp <strong>tự động hóa</strong> và cải thiện quy trình thiết kế <strong>hệ thống MEP</strong>. Xem các video hướng dẫn để tận dụng tối đa khả năng của phần mềm.</p>
            </div>

            <div className={styles.posts}>
                <div className={styles.previous} ref={navigationPrevRef}><i className="fa-solid fa-chevron-left" /></div>
                <div className={styles.next} ref={navigationNextRef}><i className="fa-solid fa-chevron-right" /></div>
                <>
                    <Swiper
                        slidesPerView={GetWinDown()}
                        spaceBetween={30}
                        autoplay={{
                            delay: 2500,
                            disableOnInteraction: false,
                        }}
                        pagination={{
                            clickable: true,
                        }}
                        modules={[Autoplay, Pagination, Navigation]}
                        className="mySwiper"
                        navigation={{
                            prevEl: navigationPrevRef.current,
                            nextEl: navigationNextRef.current,
                        }}
                        onSwiper={(swiper: any) => {
                            swiper.params.navigation.prevEl = navigationPrevRef.current
                            swiper.params.navigation.nextEl = navigationNextRef.current
                            swiper.navigation.destroy()
                            swiper.navigation.init()
                            swiper.navigation.update()
                        }}
                    >
                        {ListTutorialDB.flatMap((value: any, index: number) => {
                            return <SwiperSlide key={`tutorial` + index}>
                                <div className={styles.posted} onClick={handleOpenPreview} >
                                    <div className={styles.image}>
                                        <ReactPlayer
                                            width={"100%"}
                                            height={220}
                                            url={value.url}
                                            config={{ file: { attributes: { poster: value.image, alt: `Hình ảnh  ${value.title}` } } }}
                                        />
                                    </div>
                                    <div className={styles.mainPost}>
                                        <p className={styles.title}>{value.title}</p>
                                        <div></div>
                                        <p className={styles.description}>{value.description}</p>
                                    </div>
                                </div>
                            </SwiperSlide>
                        })}
                    </Swiper>
                </>
            </div>
            {isHiddenPreview ? <Preview handleClose={() => setIsHiddenPreview(false)} /> : <></>}
        </section>
    )
}

export default Tutorial


