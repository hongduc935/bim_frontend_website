const ListTutorialDB = [
    {
        title:"Tool Insert Family From Cad Link",
        description:"Tool này cho phép kỹ sư Bim Mep chuyển từ block cad sang Family trong model một cách nhanh chóng chỉ bằng một cú click chuột...",
        url:"https://www.youtube.com/watch?v=7-tbc5NcQqM"
    },
    {
        title:"Tool Dựng Hệ Thống MEP Từ Cad.",
        description:"Tool giúp các kỹ sư Bim Mep dựng hệ thống MEP từ file cad. Tool rất phù hợp cho tender và modelling...",
        url:"https://www.youtube.com/watch?v=CvU88ZByinI"
    },
    {
        title:"Tool kết nối thiết bị vệ sinh.",
        description:"Tool này sẽ là giả pháp giúp các kỹ sư Bim Mep nhanh chóng tạo ra những hướng đi kết nối thiết bị vệ sinh...",
        url:"https://www.youtube.com/shorts/2IugH7bYDb4"
    },
    {
        title:"Tool Auto Connect Sprinkler.",
        description:"Tool tạo ra những option kết nối đầu phun chữa cháy rất linh hoạt và dễ sử dụng...",
        url:"https://www.youtube.com/watch?v=Z4nEXc2uXwE"
    },
    {
        title:"Tool Dựng Hệ Thống MEP Từ Cad.",
        description:"Tool giúp các kỹ sư Bim Mep dựng hệ thống MEP từ file cad. Tool rất phù hợp cho tender và modelling...",
        url:"https://www.youtube.com/watch?v=CvU88ZByinI"
    },
    {
        title:"Tool kết nối thiết bị vệ sinh.",
        description:"Tool này sẽ là giả pháp giúp các kỹ sư Bim Mep nhanh chóng tạo ra những hướng đi kết nối thiết bị vệ sinh...",
        url:"https://www.youtube.com/shorts/2IugH7bYDb4"
    },
    {
        title:"Tool Auto Connect Sprinkler.",
        description:"Tool tạo ra những option kết nối đầu phun chữa cháy rất linh hoạt và dễ sử dụng...",
        url:"https://www.youtube.com/watch?v=Z4nEXc2uXwE"
    }
]
export default ListTutorialDB