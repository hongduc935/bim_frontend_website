import styles from './styles/post-new.module.scss'
import "swiper/css"
import "swiper/css/free-mode"
import "swiper/css/pagination"
import Poster1 from 'asset/images/poster1.png'
import { Swiper, SwiperSlide } from "swiper/react"
import { Autoplay, Pagination,Navigation } from "swiper"
import Image from 'components/Image/Image'
import GetWinDown from 'utility/SizeWindow'
import { useRef } from 'react'
import ListTutorialDB from './constants'
const PostNew = () => {
    const navigationPrevRef = useRef(null)
    const navigationNextRef = useRef(null)
    return (
    <div className={styles.postNew}>
        <div className={styles.title}>
            <h1 title="Cộng Đồng hỗ trợ Bim Mep tốt nhất Việt Nam">Bài viết mới nhất</h1>
        </div>
        <div className={styles.posts}>
                <div className={styles.previous} ref={navigationPrevRef}><i className="fa-solid fa-chevron-left"/></div>
                <div className={styles.next} ref={navigationNextRef}><i className="fa-solid fa-chevron-right"/></div>
                <>
                    <Swiper
                        slidesPerView={GetWinDown() ===1 ? 1 :3}
                        spaceBetween={30}
                        autoplay={{
                          delay: 2500,
                          disableOnInteraction: false,
                        }}
                        pagination={{
                          clickable: true,
                        }}
                        modules={[Autoplay, Pagination, Navigation]}
                        className="mySwiper"
                        navigation={{
                            prevEl: navigationPrevRef.current,
                            nextEl: navigationNextRef.current,
                          }}
                        onSwiper={(swiper:any) => {
                            swiper.params.navigation.prevEl = navigationPrevRef.current
                            swiper.params.navigation.nextEl = navigationNextRef.current
                            swiper.navigation.destroy()
                            swiper.navigation.init()
                            swiper.navigation.update()
                        }}
                    >
                         {ListTutorialDB.flatMap((value:any,index:number)=>{
                            return <SwiperSlide key={`postnew`+index}>
                                        <a href={value.url} target='blank'>
                                            <div className={styles.posted}>
                                                
                                                <div className={styles.image}>
                                                <Image image={Poster1}contain='contain'/>
                                                </div>
                                                <div className={styles.mainPost}>
                                                    <p className={styles.title}>{value.title}</p>
                                                    <div></div>
                                                    <p  className={styles.description}>{value.description}</p>
                                                    <button className={styles.read}>Xem hướng dẫn</button>
                                                </div>
                                            </div>  
                                        </a>    
                                    </SwiperSlide>
                         })}
                    </Swiper>
            </>     
        </div>
    </div>
  )
}

export default PostNew