import { Swiper, SwiperSlide } from "swiper/react"
import Banner1 from 'asset/images/revitmep-plugin.png'
import "swiper/css"
import "swiper/css/navigation"
import Image from 'components/Image/Image'
import styles from './styles/banner.module.scss'
import { Navigation } from "swiper";

import GetWinDown from "utility/SizeWindow";

const Banner = () => {
  return (
    <div className={styles.banner}>
        <Swiper navigation={true} modules={[Navigation]} className={`${styles.mySwiperBanner} `}>
        <SwiperSlide>
            <div className={styles.image}>
                <Image image={Banner1} contain={GetWinDown() === 1 ? "contain" :"cover" } />
            </div>
        </SwiperSlide>
      </Swiper>
    </div>
  )
}

export default Banner