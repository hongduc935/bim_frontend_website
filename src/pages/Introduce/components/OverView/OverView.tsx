import React from 'react'
import styles from './styles/overview.module.scss'
import Image from 'components/Image/Image'
const OverView = () => {
  return (
    <div className= {`${styles.overview} container`}>
        <div className={styles.title}>
            Khách hàng của chúng tôi
        </div>
        <div className={`${styles.mainOverView} item-btw w-100`}>

            <div className={`${styles.static} w-40`}>
                <div className={`${styles.staticItems} item-btw`}>
                    <div className={`${styles.item} item-btw`}>
                        <div className={`${styles.image} item-center`}>
                            <i className="fa-solid fa-users"></i>
                        </div>
                        <div className={styles.infomation}>
                            <p className={styles.title}>Users</p>
                            <p className={styles.amount}>3000 +</p>
                        </div>
                    </div>
                    <div className={`${styles.item} item-btw`}>
                        <div className={styles.image}>
                            <i className="fa-solid fa-code-pull-request"/>
                        </div>
                        <div className={styles.infomation}>
                            <p className={styles.title}>Version</p>
                            <p className={styles.amount}>3000 +</p>
                        </div>
                    </div>
                </div>
                <div className={`${styles.staticItems} item-btw`}>
                    <div className={`${styles.item} item-btw`}>
                        <div className={styles.image}>
                            <i className="fa-solid fa-star"/>
                        </div>
                        <div className={styles.infomation}>
                            <p className={styles.title}>Rating</p>
                            <p className={styles.amount}>3000 +</p>
                        </div>
                    </div>
                    <div className={`${styles.item} item-btw`}>
                        <div className={styles.image}>
                            <i className="fa-solid fa-download"/>
                        </div>
                        <div className={styles.infomation}>
                            <p className={styles.title}>Download</p>
                            <p className={styles.amount}>3000 +</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
       
    </div>
  )
}

export default OverView