import React from 'react'
import styles from './styles/main-introduce.module.scss'
const MainIntroduce = () => {
  return (
    <div className={`${styles.mainIntroduce} container`}>
        <p className={styles.title}>GIỚI THIỆU</p>
        <p className={styles.title}>KÍNH CHÀO QUÝ KHÁCH HÀNG</p>
        <p>Lời đầu tiên chúng tôi xin chân thành cảm ơn Quý khách hàng đã quan tâm đến sản phẩm và dịch vụ của <b> HQL - Revit Mep Plugins.</b>  </p>
        <p><b> HQL - Revit Mep Plugins.</b> là một đơn vị chuyên xây dựng và đào tạo kỹ sư Bim mang tầm cao mới với kỹ năng sử dụng công cụ Revit và ứng biến lập trình trong Revit. Giúp tự tin tăng tốc độ, tiết kiệm chi phí một cách tối đa cho công ty với  công việc như : dựng mô hình 3D, Combine, triển khai bản vẽ, bóc tách khối lượng Revit Mep.</p>
        <p>Ngoài ra, <b> HQL - Revit Mep Plugins.</b> còn cung cấp các giải pháp tự động hóa Revit Mep bằng bộ tool HqlTools đang được sử dụng.</p>
        <p>Với đội ngũ kỹ sư  chuyên nghiệp và tràn đầy kinh nghiệm, <b> HQL - Revit Mep Plugins.</b> luôn đem đến cho khách hàng những giải pháp tối ưu nhất để giải quyết những vấn đề về công nghệ Bim hiện nay.</p>
        <p>Với phương châm <b> "Tự Tin - Tăng Tốc - Tiết kiệm"</b> , <b> HQL - Revit Mep Plugins.</b> luôn cam kết đem đến cho khách hàng dịch vụ tốt nhất và hỗ trợ tận tình từng bước. Chúng tôi tin rằng sự hài lòng của khách hàng là niềm tự hào của chúng tôi.</p>
        <p>Chúng tôi thông tin đến Quý Khách hàng và mong nhận được sự quan tâm của Quý khách.</p>
        <p>Chúng tôi rất mong được phục vụ Quý Khách hàng trong tương lai.</p>
        <p className={styles.thank}>Trân trong cảm ơn và kính chúc sức khỏe.</p>
    </div>
  )
}

export default MainIntroduce