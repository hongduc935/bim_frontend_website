import styles from './styles/team-member.module.scss'
import Image from 'components/Image/Image'
import HQL_profile from 'asset/images/hql_profile.jpg'
import LNA_profile from 'asset/images/LNA_profile.jpg'
import HTL_profile from 'asset/images/HTL_profile.jpg'
import NMT_profile from 'asset/images/NMT_profile.png'
const TeamMember = () => {
  return (
    <div className={styles.teamMember} >
        <p className={styles.teamMemberTitle}>Đội ngũ team Developer</p>
        <p className={styles.teamMemberDescription}>Với đội ngũ nhiều năm kinh nghiệm trong ngành lập trình . Chúng tôi tự hào mang đến cho bạn những sản phẩm tốt nhất . </p>
        <div className={`${styles.listMember} item-btw`}>
            <div className={`${styles.member} `}>
                <span className={styles.linkedIn}><i className="fa-brands fa-linkedin"></i></span>
                <div className={`${styles.imageProfile} item-center`}>
                    <div className={styles.image}>
                        <Image image={HQL_profile} contain='contain'/>
                    </div>
                </div>
                <div className={`${styles.infomationProfile}`}>
                    <p className={styles.name}>Hồ Quách Lin</p>
                    <p className={styles.name1}>CEO ,Co-Founder</p>
                    <p className={styles.content}>Là thành viên hơn 10 năm kinh nghiệm trong nghành BIM MEP, đã tham gia vào nhiều dự án cao tầng ở Việt nam và nước ngoài.Tôi đã thành công trong việc tự động hóa công việc Bim Mep bằng lập trình Revit API. HqlTools,bộ Addin Revit Mep là thành quả tự hào của tôi. </p>
                </div>
            </div>
            <div className={`${styles.member} `}>
            <span className={styles.linkedIn}><i className="fa-brands fa-linkedin"></i></span>
                <div className={`${styles.imageProfile} item-center`}>
                    <div className={styles.image}>
                        <Image image={LNA_profile}/>
                    </div>
                </div>
                <div className={`${styles.infomationProfile}`}>
                    <p className={styles.name}>Lý Ngọc Âu</p>
                    <p className={styles.name1}>CEO </p>
                    <p className={styles.content}>Là thành viên có 7 năm kinh nghiệm ứng dụng BIM trong thiết kế và thi công hệ thống phòng cháy chữa cháy và cấp thoát nước công trình dân dụng và công nghiệp. Nắm và hiểu rõ các tiêu chuẩn, qui chuẩn áp dụng hiện hành trong thiết kế. </p>
                </div>
            </div>
            <div className={`${styles.member} `}>
            <span className={styles.linkedIn}><i className="fa-brands fa-linkedin"></i></span>
                <div className={`${styles.imageProfile} item-center`}>
                    <div className={styles.image}>
                        <Image image={HTL_profile}/>
                    </div>
                </div>
                <div className={`${styles.infomationProfile}`}>
                    <p className={styles.name}>Huỳnh Thanh Lâm</p>
                    <p className={styles.name1}>CFO</p>
                    <p className={styles.content}>Là kỹ sư cơ điện với hơn 12 năm kinh nghiệm trong lĩnh vực này. Tôi đã tham gia và góp phần thành công cho nhiều dự án nhà cao tầng, đã ứng dựng Revit MEP trong quá trình tối ưu hóa thiết kế và quản lý hệ thống cơ điện trong quá trình thi công.  </p>
                </div>
            </div>
            <div className={`${styles.member} `}>
            <span className={styles.linkedIn}><i className="fa-brands fa-linkedin"></i></span>
                <div className={`${styles.imageProfile} item-center`}>
                    <div className={styles.image}>
                        <Image image={NMT_profile}/>
                    </div>
                </div>
                <div className={`${styles.infomationProfile}`}>
                    <p className={styles.name}>Nguyễn Minh Tuấn</p>
                    <p className={styles.name1}>COO</p>
                    <p className={styles.content}>Là kỹ sư cơ điện với hơn 8 năm kinh nghiệm trong lĩnh vực này. Tôi đã tham gia và góp phần thành công cho nhiều dự án nhà cao tầng, đã ứng dựng Revit MEP trong quá trình tối ưu hóa thiết kế và quản lý hệ thống cơ điện trong quá trình thi công.  </p>
                </div>
            </div>

        </div>
    </div>
  )
}

export default TeamMember