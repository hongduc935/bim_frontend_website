import styles from './styles/combopackage.module.scss'

const ComboPackage = () => {
  return (
    <> 
    <div className={styles.titleCombo}>
        <p className={styles.title1}>Các gói dịch vụ ưu đãi </p>
        <p className={styles.title2}>Giải pháp tối ưu chi phí cho 1 ứng dụng Revit</p>
    </div>
    <div className={`${styles.coomboPackage} container item-center`}>
    <div className={`${styles.mainCombo} item-btw w-80`}>
        <div className={styles.itemCombo}>
                <p className={styles.feeTitle}>FREE</p>
                <p className={styles.feeMoney}>0 Đ</p>
                <ul className={styles.listFeature}>
                    <li><i className="fa-solid fa-check"/> &nbsp; Miễn phí sử dụng</li>
                    <li><i className="fa-solid fa-check"/>&nbsp; Luôn cập nhật version mới</li>
                    <li><i className="fa-solid fa-check"/>&nbsp; Sử dụng Full chức năng</li>
                    <li><i className="fa-solid fa-check"/>&nbsp; Thời hạn Trial 1 tháng</li>
                    <li><i className="fa-solid fa-check"/>&nbsp; Không giới hạn thời gian mở tool</li>
                    <li><i className="fa-solid fa-check"/>&nbsp; Hỗ trợ 24/24</li>
                    <li><i className="fa-solid fa-check"/>&nbsp; Hướng dẫn cài đặt </li>
                    <li><i className="fa-solid fa-check"/>&nbsp; Hướng dẫn sử dụng</li>
                </ul>
        </div>
        <div className={styles.itemCombo}>
                <p className={styles.feeTitle}>6 Tháng</p>
                <p className={styles.feeMoney}>550 000 Đ</p>
                <ul className={styles.listFeature}>
                    <li><i className="fa-solid fa-check"/> &nbsp; Miễn phí sử dụng</li>
                    <li><i className="fa-solid fa-check"/>&nbsp; Luôn cập nhật version mới</li>
                    <li><i className="fa-solid fa-check"/>&nbsp; Sử dụng Full chức năng</li>
                    <li><i className="fa-solid fa-check"/>&nbsp; Thời hạn 6 tháng</li>
                    <li><i className="fa-solid fa-check"/>&nbsp; Không giới hạn thời gian mở tool</li>
                    <li><i className="fa-solid fa-check"/>&nbsp; Hỗ trợ 24/24</li>
                    <li><i className="fa-solid fa-check"/>&nbsp; Hướng dẫn cài đặt </li>
                    <li><i className="fa-solid fa-check"/>&nbsp; Hướng dẫn sử dụng</li>
                </ul>
        </div>
        <div className={styles.itemCombo}>
                <p className={styles.feeTitle}>1 Năm</p>
                <p className={styles.feeMoney}>960 000 Đ</p>
                <ul className={styles.listFeature}>
                    <li><i className="fa-solid fa-check"/> &nbsp; Miễn phí sử dụng</li>
                    <li><i className="fa-solid fa-check"/>&nbsp; Luôn cập nhật version mới</li>
                    <li><i className="fa-solid fa-check"/>&nbsp; Sử dụng Full chức năng</li>
                    <li><i className="fa-solid fa-check"/>&nbsp; Thời hạn 12 tháng </li>
                    <li><i className="fa-solid fa-check"/>&nbsp; Không giới hạn thời gian mở tool</li>
                    <li><i className="fa-solid fa-check"/>&nbsp; Hỗ trợ 24/24</li>
                    <li><i className="fa-solid fa-check"/>&nbsp; Hướng dẫn cài đặt </li>
                    <li><i className="fa-solid fa-check"/>&nbsp; Hướng dẫn sử dụng</li>
                </ul>
        </div>

    </div>
   
   
</div>
    </>
   
  )
}

export default ComboPackage