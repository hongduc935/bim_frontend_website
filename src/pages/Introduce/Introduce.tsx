import ComponentPage from 'components/ComponentPage/ComponentPages'
import React from 'react'
import styles from './styles/introduce.module.scss'
import BreakCrumb from 'components/BreakCrumb/BreakCrumb'
import MainIntroduce from './components/MainIntroduce/MainIntroduce'
import ComboPackage from './components/ComboPackage/ComboPackage'
import TeamMember from './components/TeamMember/TeamMember'
import OverView from './components/OverView/OverView'

const Introduce = () => {
  return (
    <div className={styles.introduce}>
      <ComponentPage isHiddenHeader={true} isHiddenFooter={true}>
        <BreakCrumb content = "Trang Chủ / Giới Thiệu"/>
        <MainIntroduce/>
       {/*   <ComboPackage/>*/}
        <TeamMember/>
        {/* <OverView/> */}
      </ComponentPage>
    </div>
  )
}

export default Introduce