import styles from './styles/main-page.module.scss'
import PaperTrending from '../PaperTrending/PaperTrending'
import PaperPage from '../PaperPage/PaperPage'


const MainPage = () => {
  return (
    <div className={`${styles.mainPage} container item-center`}>
        <div className={`${styles.left} w-70`}>
            <PaperPage/> 
        </div>
        <div className={`${styles.right} w-30`}>
            <PaperTrending/>
        </div>
  
    </div>
  )
}

export default MainPage