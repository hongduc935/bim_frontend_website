import styles from './styles/paper-trending.module.scss'
import Image from 'components/Image/Image'
const PaperTrending = () => {
  return (
    <div className={styles.paperTrending}>
        <div className={styles.title}>
            TIN XEM NHIỀU
        </div>
        <div className={`${styles.listBtn}`}>
            <button> Tuần này</button>
            <button> Tháng này</button>
            <button> Mọi lúc</button>
        </div>
        <div className={styles.listItem}>
            <div className={`${styles.cartIT} w-100 item-btw`}>
                <div className={`${styles.img} `}>
                    <Image image='https://bimvietsolutions.com/uploads/images/202302/image_140x98_63dcc7b536169.jpg'  />
                </div>
                <div className={`${styles.info} `}>
                        <p>Hướng dẫn đăng kí tài khoản</p>
                        <p className={styles.postMeta}>
                            <span>Jan 17, 2023</span>
                            <span> <i className="fa-solid fa-comment"></i> 0 </span>
                            <span> <i className="fa-solid fa-eye"/> 0</span>
                        </p>
                </div>

            </div>
            <div className={`${styles.cartIT} w-100 item-btw`}>
                <div className={`${styles.img} `}>
                    <Image image='https://bimvietsolutions.com/uploads/images/202302/image_140x98_63dcc7b536169.jpg'  />
                </div>
                <div className={`${styles.info} `}>
                        <p>Hướng dẫn đăng kí tài khoản</p>
                        <p className={styles.postMeta}>
                            <span>Jan 17, 2023</span>
                            <span> <i className="fa-solid fa-comment"></i> 0 </span>
                            <span> <i className="fa-solid fa-eye"/> 0</span>
                        </p>
                </div>

            </div>
            <div className={`${styles.cartIT} w-100 item-btw`}>
                <div className={`${styles.img} `}>
                    <Image image='https://bimvietsolutions.com/uploads/images/202302/image_140x98_63dcc7b536169.jpg'  />
                </div>
                <div className={`${styles.info} `}>
                        <p>Hướng dẫn đăng kí tài khoản</p>
                        <p className={styles.postMeta}>
                            <span>Jan 17, 2023</span>
                            <span> <i className="fa-solid fa-comment"></i> 0 </span>
                            <span> <i className="fa-solid fa-eye"/> 0</span>
                        </p>
                </div>

            </div>
            <div className={`${styles.cartIT} w-100 item-btw`}>
                <div className={`${styles.img} `}>
                    <Image image='https://bimvietsolutions.com/uploads/images/202302/image_140x98_63dcc7b536169.jpg'  />
                </div>
                <div className={`${styles.info} `}>
                        <p>Hướng dẫn đăng kí tài khoản</p>
                        <p className={styles.postMeta}>
                            <span>Jan 17, 2023</span>
                            <span> <i className="fa-solid fa-comment"></i> 0 </span>
                            <span> <i className="fa-solid fa-eye"/> 0</span>
                        </p>
                </div>

            </div>
            <div className={`${styles.cartIT} w-100 item-btw`}>
                <div className={`${styles.img} `}>
                    <Image image='https://bimvietsolutions.com/uploads/images/202302/image_140x98_63dcc7b536169.jpg'  />
                </div>
                <div className={`${styles.info} `}>
                        <p>Hướng dẫn đăng kí tài khoản</p>
                        <p className={styles.postMeta}>
                            <span>Jan 17, 2023</span>
                            <span> <i className="fa-solid fa-comment"></i> 0 </span>
                            <span> <i className="fa-solid fa-eye"/> 0</span>
                        </p>
                </div>

            </div>
        </div>
        <div className={`${styles.title} ${styles.titleMt}`}>
            Thẻ Phổ biến
        </div>
        <div className={styles.tagPopular}>
            <div className={styles.line}>
                <p> Năng lực chuyên gia</p>
                <p> Bim Biệt</p>
                <p> Giải pháp chuyển đổi số</p>
                <p> Chuyển đổi số HQLS Plugin TOOL</p>
                <p> Digital CONVERSION</p>
                <p> Giải pháp lập trình ứng dụng trên thiết bị di động</p>
                <p>Apple Store</p>
                <p>Mobile Develop</p>
                <p>Lập trình di động</p>
                <p>CHPLAY</p>
                <p>OpenAI</p>
                <p>Chat GPT</p>
                <p>Chrome Plugin</p>
                <p>Chrome Extension</p>
                <p> Best Chrome Extension</p>
            </div>
        </div>
    </div>
  )
}

export default PaperTrending