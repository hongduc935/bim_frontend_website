import Image from 'components/Image/Image'
import styles from './styles/paper-page.module.scss'

const PaperPage = () => {
  return (
    <div className={styles.paperPage}>
         <div className={styles.title}> <i className="fa-solid fa-grid-2 m-r-5"></i> Tin Tức</div>
         <div className={styles.grid}>
            <div className={styles.cart}>
                <div className={styles.image}>
                    <Image image='https://bimvietsolutions.com/uploads/images/202302/image_960x_63db75bb2153f.jpg' contain='cover'/>
                </div>
                <div className={styles.mainPost}>
                    <p className={styles.title1}>Dịch vụ tư vấn chuyển đổi số</p>
                    <p className={styles.postMeta}>
                        <span>Jan 17, 2023</span>
                        <span> <i className="fa-solid fa-comment"></i> 0 </span>
                        <span> <i className="fa-solid fa-eye"/> 0</span>
                    </p>
                    <p  className={styles.description}>Tư vấn những giải pháp phù hợp nhằm giúp cho cá nhân, doanh nghiệp thuận lợi trong việc chuyển đổi cơ sở dữ liệu, quy trình...</p>
                </div>
            </div>
            <div className={styles.cart}>
                <div className={styles.image}>
                    <Image image='https://bimvietsolutions.com/uploads/images/202302/image_380x226_63dcc6157cb9e.jpg' contain='cover'/>
                </div>
                <div className={styles.mainPost}>
                    <p className={styles.title1}>Dịch vụ tư vấn chuyển đổi số</p>
                    <p className={styles.postMeta}>
                        <span>Jan 17, 2023</span>
                        <span> <i className="fa-solid fa-comment"></i> 0 </span>
                        <span> <i className="fa-solid fa-eye"/> 0</span>
                    </p>
                    <p  className={styles.description}>Tư vấn những giải pháp phù hợp nhằm giúp cho cá nhân, doanh nghiệp thuận lợi trong việc chuyển đổi cơ sở dữ liệu, quy trình...</p>
                </div>
            </div>
            <div className={styles.cart}>
                <div className={styles.image}>
                    <Image image='https://bimvietsolutions.com/uploads/images/202301/image_380x226_63baa340337e0.jpg' contain='cover'/>
                </div>
                <div className={styles.mainPost}>
                    <p className={styles.title1}>Dịch vụ tư vấn chuyển đổi số</p>
                    <p className={styles.postMeta}>
                        <span>Jan 17, 2023</span>
                        <span> <i className="fa-solid fa-comment"></i> 0 </span>
                        <span> <i className="fa-solid fa-eye"/> 0</span>
                    </p>
                    <p  className={styles.description}>Tư vấn những giải pháp phù hợp nhằm giúp cho cá nhân, doanh nghiệp thuận lợi trong việc chuyển đổi cơ sở dữ liệu, quy trình...</p>
                </div>
            </div>
            <div className={styles.cart}>
                <div className={styles.image}>
                    <Image image='https://bimvietsolutions.com/uploads/images/202301/image_380x226_63b90c38dab33.jpg' contain='cover'/>
                </div>
                <div className={styles.mainPost}>
                    <p className={styles.title1}>Dịch vụ tư vấn chuyển đổi số</p>
                    <p className={styles.postMeta}>
                        <span>Jan 17, 2023</span>
                        <span> <i className="fa-solid fa-comment"></i> 0 </span>
                        <span> <i className="fa-solid fa-eye"/> 0</span>
                    </p>
                    <p  className={styles.description}>Tư vấn những giải pháp phù hợp nhằm giúp cho cá nhân, doanh nghiệp thuận lợi trong việc chuyển đổi cơ sở dữ liệu, quy trình...</p>
                </div>
            </div>
        </div>
    </div>
  )
}

export default PaperPage