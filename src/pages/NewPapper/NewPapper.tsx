import styles from './styles/new-paper.module.scss'
import ComponentPage from 'components/ComponentPage/ComponentPages'
import BreakCrumb from 'components/BreakCrumb/BreakCrumb'
import MainPage from './components/MainPage/MainPage'


const NewPapper = () => {
  return (
    <div className={styles.newPapper}>
      <ComponentPage isHiddenHeader={true} isHiddenFooter={true}>
        <BreakCrumb content = "Trang Chủ / Tin Tức"/>
        <MainPage/>
      </ComponentPage>
    </div>
  )
}

export default NewPapper