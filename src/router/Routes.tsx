import { BrowserRouter } from "react-router-dom"
import PublicRoutes from './PublicRoutes'
export default function Routers() {
    return (
        <BrowserRouter>
            <PublicRoutes/>
        </BrowserRouter>
    )
}