// import moment from "moment"

export const convertDate = (value:any) => {
    
    let date = new Date(value)
     return date.getFullYear() +"-"+ (date.getMonth()+1) +"-"+ date.getDate();
    // return moment(value?.toString()).format('YYYY-MM-DD').toString()
    // console.log(typeof value)
    // console.log(value)
    // return value;
}

export const convertDateLocal = (value:any) => {
    // return moment(value?.toString()).lo
}
// export const convertTimeDate = (value:any) => {
//     return moment(value).format('HH:MM DD/MM/YYYY')
// }

export const convertDateTime = (time:any) => {
    let hours:string = new Date(time).getHours().toString()
    let minutes:string = new Date(time).getMinutes().toString()
    let date:string = new Date(time).getDate().toString()
    let month:string = (new Date(time).getMonth() + 1).toString()
    const year = new Date(time).getFullYear()
    if (Number(hours) < 10) {
        hours = `0${hours}`
    }
    if (Number(minutes) < 10) {
        minutes = `0${minutes}`
    }
    if (Number(date) < 10) {
        date = `0${date}`
    }
    if (Number(month) < 10) {
        month = `0${month}`
    }
    return `${date}/${month}/${year}  ${hours}:${minutes}`
}
