import {ILogin, IRegister} from "@type/AuthInterface"
import {Dispatch} from "redux"
import AuthServices from "api/auth.api"

import {toast, ToastContainer} from "react-toastify"
import * as actionTypes from '../../constant/Auth/Auth'
import * as notifyTypes from '../../constant/Notify/Notify'

export const LoginAction=(user:ILogin)=> async (dispatch:Dispatch)=>{
    try {
        dispatch({
            type:notifyTypes.LOADING
        })
        return await AuthServices.postUserLogin(user)
            .then(
                (result:any)=>{
                    localStorage.setItem("user",result.data.access_token)
                    localStorage.setItem("info-user", JSON.stringify(result.data.user))
                    dispatch({
                        type:actionTypes.AUTH_LOGIN_SUCCESS,
                        payload:result.data
                    })
                    dispatch({
                        type:notifyTypes.NOT_LOADING
                    })
                    return Promise.resolve();
                }
            )
            .catch((err)=>{
                dispatch({
                    type:actionTypes.AUTH_LOGIN_FAIL,
                    payload:err
                })
                dispatch({
                    type:notifyTypes.NOT_LOADING
                })
                return Promise.reject();
            })

    }catch (err:any){
        dispatch({
            type:notifyTypes.NOT_LOADING
        })
        dispatch({
            type:actionTypes.AUTH_LOGIN_FAIL,
            payload:err
        })
        return err.message
    }
}


export const RegisterAction=(userRegister:IRegister)=> async (dispatch:Dispatch)=>{
    try {
        dispatch({
            type:notifyTypes.LOADING
        })
        return  await AuthServices.postUserRegister(userRegister)
            .then(
                (result:any)=>{
                    let data:any = result?.data

                    let statusCode = data?.statusCode

                    if(statusCode ===2000){
                        toast(data.msg)
                    }
                    else{
                        toast("Đăng kí tài khoản thành công.")
                        dispatch({
                            type:actionTypes.AUTH_REGISTER_SUCCESS,
                            payload:result.data
                        })
                        dispatch({
                            type:notifyTypes.NOT_LOADING
                        })
                        setTimeout(()=>{
                            window.location.href = "/"
                        },3500)
                        
                    }
                    return Promise.resolve();
                }
            )
            .catch((err)=>{
                dispatch({
                    type:actionTypes.AUTH_REGISTER_FAIL,
                    payload:err
                })
                dispatch({
                    type:notifyTypes.NOT_LOADING
                })
                return Promise.reject();
            })

    }catch (err:any){
        dispatch({
            type:actionTypes.AUTH_REGISTER_FAIL,
            payload:err
        })
        dispatch({
            type:notifyTypes.NOT_LOADING
        })
        return err.message
    }
}

export const LogoutAction=()=> async ()=>{
         await localStorage.removeItem("user")
         await AuthServices.logout()
}

