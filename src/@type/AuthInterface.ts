export interface ILogin{
  email:string
  password:string,
}
export interface ILogin{
  email:string
  password:string,
}
export interface IUser extends ILogin{
  username:string,
  phone:string,
  addressUser:string,
  access_token:string
  role:string
  id:string
}
export interface IRegister extends ILogin{
  name:string
  phone:string
  packages:string
  confirm_password:string
}
